var isMobile = isUserAgentCheck(["iPhone", "iPad", "iPod", "android", "blackberry","CriOS","Mobile"]);
var isSmallMobile = isUserAgentCheck(["iPhone", "iPod", "android", "blackberry"]);
var isApple = isUserAgentCheck(["iPhone", "iPad", "iPod"]);
var isIphone = isUserAgentCheck(["iPhone"]);
var isAndroid = isUserAgentCheck(["android"]);
var isYandex = isUserAgentCheck(['YaBrowser']);
var isIpadSafariIos7 = navigator.userAgent.match(/iPad;.*CPU.*OS 7_\d/i) && !isUserAgentCheck(["CriOS"]);
var ieNum = (function(){
                var undef,v = 3,div = document.createElement('div'),all = div.getElementsByTagName('i');
                while (div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',all[0]);
                return v > 4 ? v : 100500;
            }());
if (ieNum == 100500 && isUserAgentCheck(['Trident'])) { ieNum = 10; }

var isTouchSupported = 'ontouchstart' in window;

if (isMobile) {
    if (isSmallMobile) {
        document.write('<meta name="viewport" content="initial-scale=0.5, width=device-width, minimal-ui" />');
    } else {
        document.write('<meta name="viewport" content="initial-scale=0.65, width=device-width" />');
    }
}

// ********************** document classes ****************************

var htmlNode = document.html || document.getElementsByTagName('html')[0];
htmlNode.className = htmlNode.className + ' ' +
                     (isMobile ? 'html-mobile ' : '') +
                     (isSmallMobile ? 'html-phone ' : '') +
                     (isApple ? 'html-apple ' : '') +
                     (isIphone ? 'html-iphone ' : '') +
                     (isAndroid ? 'html-android ' : '') +
                     (isYandex ? 'html-yandex ' : '') +
                     (isIpadSafariIos7 ? 'html-ipadsafariios7 ' : '') +
                     (isTouchSupported ? 'html-touch ' : '') +
                     ((ieNum < 100500) ? 'html-ie html-ie'+ieNum+' ' : '') +
                     ((ieNum <= 8) ? 'html-ie_old ' : '');

// ********************** console log fix ie *****************************

(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

function isUserAgentCheck(ua){
    var agent = navigator.userAgent;
    for(var i = 0; i < ua.length; i++)
        if(Boolean(agent.match(new RegExp(ua[i],"i"))))
            return true;
    return false;
}
