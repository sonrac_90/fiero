/*********************************************** NAVIGATION *******************************************************/

var NOW = 'preloader';
var page_changing = false;
var pages_title_suffix = ' — Fiero Animals';

/**
 * Существует ли страница с таким именем
 */
function pagesIdByName(page_name) {
    for (var i = 0; i < pages.length; i++) {
        if (pages[i]['name'] === page_name) {
            return i;
        }
    }
    return -1;
}

/**
 * Запускается один раз после загрузки страницы
 * ищет, нет ли в истории или в адресе данных о странице, которую нужно загрузить
 * в противном случае загружает главную
 * Загружает не явно, а через History.pushState
 */
function history_load() {
    var state = History.getState();

    // в истории уже есть сохраненные данные о странице
    if (typeof state.data.now !== 'undefined') {
        history_change_state(false);
    }
    // данные о странице есть в адресе
    else if (typeof CURRENT_PAGE !== 'undefined' && CURRENT_PAGE !== '') {
        var pages_title = '';
        if (pagesIdByName(CURRENT_PAGE) > -1) {
            pages_title = pages[pagesIdByName(CURRENT_PAGE)]['title'];
        }

        History.pushState({ 'now' : CURRENT_PAGE }, pages_title + pages_title_suffix, baseUrl + '/' + CURRENT_PAGE);
    }
    // вообще нигде никаких данных не найдено
    else {
        History.pushState({ 'now' : 'index' }, pages[pagesIdByName('index')]['title'] + pages_title_suffix, baseUrl + '/index');
    }
}

/**
 * Обертка над History.pushState
 */
function set_page(page_name, direction) {
    if (!page_changing && app_started)
    {
        var data_obj = {
            'now' : page_name
        };

        if (typeof direction !== 'undefined' && direction !== '') {
            data_obj.direction = direction;
        }

        var pages_title = '';
        if (pagesIdByName(page_name) > -1) {
            pages_title = pages[pagesIdByName(page_name)]['title'];
        }

        History.pushState(data_obj, pages_title + pages_title_suffix, baseUrl + '/' + page_name);
    }
}

/**
 * Вызывается каждый раз при изменении состояния объекта History
 * является единственной точкой входа для change_page
 */
function history_change_state(event) {
    var state = History.getState();

    // Если такая страница существует вообще
    if (typeof state.data.now !== 'undefined' && pagesIdByName(state.data.now) > -1) {
        document.title = pages[pagesIdByName(state.data.now)]['title'] + pages_title_suffix;
        change_page();
    } else {
        History.replaceState({ 'now' : 'index' }, pages[pagesIdByName('index')]['title'] + pages_title_suffix, baseUrl + '/index');
    }
}