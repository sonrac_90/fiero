/**
 *
 *  @preserve Copyright 2014
 *  Version: 0.22
 *  Licensed under GPLv2.
 *  Usage: $('.slider').megaslider(options);
 *
 */
(function($){
    $.fn.megaslider = function(opt)
    {
        var options = $.extend({
            slider_content_class: '',
            slider_content_active_class: '',

            slider_button_container_class: '',
            slider_button_class: '',
            slider_button_active_class: '',

            slider_arrow_class: '',
            slider_arrow_left_class: '',
            slider_arrow_right_class: '',
            slider_arrow_disabled_class: '',

            'pause_time' : 5000,
            'transition_time' : 1000,
            'transition_type' : 'margin', // margin | margin-shift
            'autostart' : true
        }, opt);

        return this.each(function(index, jqitem)
        {
            var j_item = $(jqitem);
            var slider_changing = false;

            /******************************* SLIDER FUNCTIONS **********************************/

            var slider_change = function(direction) {
                if (typeof direction !== 'undefined' && !slider_changing) {
                    clearInterval(slider_timeout);
                    if (direction == 'prev') {
                        home_products_prev();
                    } else if (direction == 'next') {
                        home_products_next();
                    } else if (parseInt(direction) >= 0) {
                        home_products_set(parseInt(direction));
                    }
                }
            };

            var home_products_prev = function() {
                var curr_prod_num = 1;
                if (parseInt(j_item.find('.'+options.slider_content_active_class).attr('data-group_num')) > 0) {
                    curr_prod_num = parseInt(j_item.find('.'+options.slider_content_active_class).attr('data-group_num'));
                }

                if (curr_prod_num > 1) {
                    home_products_set(curr_prod_num-1);
                } else {
                    home_products_set(j_item.find('.'+options.slider_content_class).length, 'left');
                }
            };

            var home_products_next = function() {
                var curr_prod_num = 1;
                if (parseInt(j_item.find('.'+options.slider_content_active_class).attr('data-group_num')) > 0) {
                    curr_prod_num = parseInt(j_item.find('.'+options.slider_content_active_class).attr('data-group_num'));
                }

                if (curr_prod_num < j_item.find('.'+options.slider_content_class).length) {
                    home_products_set(curr_prod_num+1);
                } else {
                    home_products_set(1, 'right');
                }
            };

            var home_products_set = function(prod_num, direction) {
                direction = direction || '';

                var old_prod_num = 1;
                if (parseInt(j_item.find('.'+options.slider_content_active_class).attr('data-group_num')) > 0) {
                    old_prod_num = parseInt(j_item.find('.'+options.slider_content_active_class).attr('data-group_num'));
                }

                if (old_prod_num != prod_num)
                {
                    slider_changing = true;
                    j_item.addClass('js-megaslider_changing');

                    var old_end_position = 0;
                    var new_start_position = 0;

                    // ------------------------------------ transition ----------------------------------
                    if (direction !== '') {
                        if (direction === 'left') {
                            old_end_position = '100%';
                            new_start_position = '-100%';
                        } else if (direction === 'right') {
                            old_end_position = '-100%';
                            new_start_position = '100%';
                        }
                    } else {
                        if (old_prod_num < prod_num) {
                            old_end_position = '-100%';
                            new_start_position = '100%';
                        } else if (prod_num < old_prod_num) {
                            old_end_position = '100%';
                            new_start_position = '-100%';
                        }
                    }

                    if (options.transition_type == 'margin') {

                        j_item.find('.'+options.slider_content_class+'_'+prod_num).css({ 'left' : new_start_position });
                        animateIt(j_item.find('.'+options.slider_content_class+'_'+old_prod_num), {'left' : old_end_position}, options.transition_time, '', 0);
                        animateIt(j_item.find('.'+options.slider_content_class+'_'+prod_num), {'left' : '0'}, options.transition_time, '', 0, function(){
                            j_item.find('.'+options.slider_content_class+'_'+old_prod_num).css({ 'left' : '100%' });
                            slider_changing = false;
                            j_item.removeClass('js-megaslider_changing');
                        });
                    }
                    else if (options.transition_type == 'margin-shift') {
                        j_item.find('.'+options.slider_content_class+'_'+prod_num).css({ 'left' : new_start_position });
                        j_item.find('.'+options.slider_content_class+'_'+old_prod_num).css({ 'z-index' : 1 });
                        animateIt(j_item.find('.'+options.slider_content_class+'_'+prod_num), {'left' : '0'}, 600, 'ease', 0);
                        animateIt(j_item.find('.'+options.slider_content_class+'_'+old_prod_num), {'left' : old_end_position}, 700, 'ease-in-out', 0, function(){
                            j_item.find('.'+options.slider_content_class+'_'+old_prod_num).css({ 'left' : '100%', 'z-index' : '' });
                            slider_changing = false;
                            j_item.removeClass('js-megaslider_changing');
                        });
                    }
                    // ----------------------------------- #transition ----------------------------------

                    j_item.find('.'+options.slider_content_class).removeClass(options.slider_content_active_class);
                    j_item.find('.'+options.slider_content_class+'_'+prod_num).addClass(options.slider_content_active_class);

                    j_item.find('.'+options.slider_button_class).removeClass(options.slider_button_active_class);
                    if (j_item.find('.'+options.slider_button_class+'[data-group_num='+prod_num+']').length > 0) {
                        j_item.find('.'+options.slider_button_class+'[data-group_num='+prod_num+']').addClass(options.slider_button_active_class);
                    }

                    if (options.slider_button_container_class !== '') {
                        $(j_item).find('.'+options.slider_button_container_class).attr('class', options.slider_button_container_class+' '+options.slider_button_container_class+'_'+prod_num);
                    }
                }
            };

            /******************************** INIT SLIDER ***************************************/

            $(j_item).find('.'+options.slider_content_class).each(function(idx, j_content){
                $(j_content).addClass(options.slider_content_class+'_'+(idx+1)).attr('data-group_num', (idx+1));
                if (idx == 0) {
                    $(j_content).addClass(options.slider_content_active_class);
                } else {
                    $(j_content).css({ 'left' : '100%' });
                }
            });

            if (options.slider_button_class !== '') {
                $(j_item).find('.'+options.slider_button_class).each(function(idx, j_button){
                    $(j_button).addClass(options.slider_button_class+'_'+(idx+1)).attr('data-group_num', (idx+1));
                    if (idx == 0) {
                        $(j_button).addClass(options.slider_button_active_class);
                    }
                    if (idx < $('.'+options.slider_content_class).length) {
                        $(j_button).click(function(){
                            slider_change(idx+1);
                        });
                    }
                });
            }
            if (options.slider_button_container_class !== '') {
                $(j_item).find('.'+options.slider_button_container_class).addClass(options.slider_button_container_class+'_1');
            }

            if (options.slider_arrow_left_class !== '') {
                $(j_item).find('.'+options.slider_arrow_left_class).click(function(){
                    slider_change('prev');
                });
            }
            if (options.slider_arrow_right_class !== '') {
                $(j_item).find('.'+options.slider_arrow_right_class).click(function(){
                    slider_change('next');
                });
            }

            // slider interval
            var slider_timeout = 0;
            if (options.autostart) {
                slider_timeout = setInterval(home_products_next, options.pause_time);
            }
        });
    };
})(jQuery);