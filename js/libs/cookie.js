function CookieFlag(cookieName){

    var _flag = null;
    getCookie();

    function getCookie() {
        if (typeof cookieName !== "undefined") {
            setFlag(getCookieState());
        } else {
            setFlag(false);
        }
    }

    function getCookieState() {
        var c_name = cookieName;

        if (document.cookie.length > 0) {
            var c_start = document.cookie.indexOf(c_name + "=");
            if (c_start != -1) {
                c_start = c_start + c_name.length + 1;
                var c_end = document.cookie.indexOf(";", c_start);
                if (c_end == -1) c_end = document.cookie.length;

                if (unescape(document.cookie.substring(c_start, c_end)) == 'true') return true;
                else return false;
            }
        }
        return false;
    }
    function toggleFlag(){
        setFlag(!_flag);
    }
    function setFlag(flag){
        _flag = flag;
        if (document.cookie && (typeof cookieName !== "undefined")) {
            document.cookie = cookieName + '=' + _flag + ';';
        }
    }
    function getFlag(){
        return _flag;
    }

    return{
        setFlag : setFlag,
        getFlag : getFlag,
        toggleFlag : toggleFlag
    }

}