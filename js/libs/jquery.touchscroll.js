/**
 *
 *  @preserve Copyright 2013
 *  Version: 0.1
 *  Licensed under GPLv2.
 *  Usage: $('div').touchscroll(function(event, delta, deltaX, deltaY){ ... });
 *
 */
(function($){
    $.fn.touchscroll = function(callback, options)
    {
        var isTouchSupported = 'ontouchstart' in window;

        options = $.extend({
            prevent_default: false
        }, options);

        return this.each(function(index, jqitem)
        {
            var j_item = $(jqitem);
            var j_drag = false;

            var j_start_left_mouse = 0;
            var j_start_top_mouse = 0;
            var j_finish_left_mouse = 0;
            var j_finish_top_mouse = 0;

            var min_scroll = 100;

            if (isTouchSupported)
            {
                j_item.get(0).addEventListener('touchstart', function(e) {

                    if (!j_drag) {
                        j_drag = true;
                        var touch = e.changedTouches[0];

                        j_start_left_mouse = touch.clientX;
                        j_start_top_mouse = touch.clientY;
                    }

                }, false);

                j_item.get(0).addEventListener('touchmove', function(e) {

                    if (j_drag) {
                        if (options.prevent_default) {
                            e.preventDefault();
                        }

                        var touch = e.changedTouches[0];
                        j_finish_left_mouse = j_start_left_mouse - touch.clientX;
                        j_finish_top_mouse = j_start_top_mouse - touch.clientY;

                        //$('.b-content_title').html(j_start_left_mouse + ' ' + j_finish_left_mouse + ' ' +  j_start_top_mouse + ' ' + j_finish_top_mouse);

                        if (Math.abs(j_finish_top_mouse) >= min_scroll || Math.abs(j_finish_left_mouse) >= min_scroll) {
                            j_drag = false;

                            if (typeof callback === 'function') {
                                var deltaX = Math.floor(j_finish_left_mouse / (min_scroll / 2));
                                var deltaY = Math.floor(j_finish_top_mouse / (min_scroll / 2));

                                var delta = 0;
                                if (Math.abs(deltaY) > 1 && Math.abs(deltaY) > Math.abs(deltaX)) { delta = deltaY; }
                                else if (Math.abs(deltaX) > 1 && Math.abs(deltaX) > Math.abs(deltaY)) { delta = deltaX; }
                                else if (Math.abs(deltaX) > 0) { delta = deltaX; }
                                else if (Math.abs(deltaY) > 0) { delta = deltaY; }

                                // по другому не будет работать =\
                                $.ajax({
                                    url: 'http://localhost/',
                                    timeout: 1,
                                    complete : function() {
                                        callback(e, -delta, -deltaX, -deltaY);
                                    }
                                });
                            }
                        }
                    }

                });

                j_item.get(0).addEventListener('touchend', function(e) {

                    if (j_drag) {
                        j_drag = false;
                    }

                }, false);
            }

            /***************************************************************************************************/
        });
    };
})(jQuery);