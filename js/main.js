var app_started = false;

$(document).ready(function()
{
    // on window scroll
    $(window).scroll(window_scroll);

    // resizers
    $(window).resize(bg_on_resize);
    bg_img_onload();
    bg_on_resize();
    setInterval(bg_on_resize, 2000);

    // orientation for mobile devices
    if (ieNum > 11) {
        updateOrientation();
        window.addEventListener('orientationchange', updateOrientation, false);
    }

    // sliders
    index_slider_init();
    work_slider_init();

    // app start
    app_start();
});

/************************************************* COMMON ******************************************************/

function window_scroll() {
    if ($(window).scrollTop() > 60) {
        grey_menu_set_fixed();
    } else if ($(window).scrollTop() <= 60) {
        grey_menu_remove_fixed();
    }
}

function bg_on_resize() {
    var window_height = $(window).height();
    var header_height = $('.b-header').height();
    $('.b-content_100').css({ 'height' : (window_height - header_height + 1) });

    bg_img_update();
    fonts_autosize();
    auto_aspect_ratio();
    update_scale();
}

function bg_img_update() {
    $('.bg').resizer();
}

function bg_img_onload() {
    $('img.bg').load(function(){
        $(this).resizer();
    });
}

function updateOrientation() {
    switch (window.orientation) {
        case 0: case 180: //portrait
            $('html').removeClass('html-landscape').addClass('html-portrait');
        break;
        case 90: case -90: //landscape
            $('html').removeClass('html-portrait').addClass('html-landscape');
        break;
        default:
            $('html').removeClass('html-portrait').removeClass('html-landscape');
        break;
    }
}

function update_scale() {
    var window_width = $('body').width();
    var window_height = $('body').height();

    if (!isMobile) {
        if (window_width < 1300 || window_height < 900) {
            $('html').addClass('html-scale');
        } else {
            $('html').removeClass('html-scale');
        }
    }
}

function animateIt(selector, css, time, easing, delay, callback) {
    if (ieNum > 9) {
        var _css = css;
        var _css_arr = Object.keys(css);

        _css['-webkit-transition'] = '';
        _css['transition'] = '';
        for (var i = 0; i < _css_arr.length; i++) {
            _css['-webkit-transition'] += ((i > 0) ? ', ' : '') + _css_arr[i] + ' ' + time + 'ms ' + ((easing == '') ? 'cubic-bezier(0.215, 0.61, 0.355, 1)' : easing);
            _css['transition'] += ((i > 0) ? ', ' : '') + _css_arr[i] + ' ' + time + 'ms ' + ((easing == '') ? 'cubic-bezier(0.215, 0.61, 0.355, 1)' : easing);
        }

        setTimeout(function () {
            $(selector).css(_css);

            setTimeout(function () {
                $(selector).css({ '-webkit-transition' : '', 'transition' : '' });
                if (typeof callback === 'function') { callback(); }
            }, time + 50);
        }, delay + 50);
    } else {
        $(selector).css(css);
        if (typeof callback === 'function') { callback(); }
    }
}

function fonts_autosize() {
    $('.font-autosize').each(function(index, jqitem){
        var parent_real_width = parseInt($(jqitem).parent().attr('r-width'));
        var parent_width = $(jqitem).parent().width();
        var width_koef = parent_width / parent_real_width;
        $(jqitem).css({'font-size' : Math.max(11, ((width_koef * parseInt($(jqitem).attr('f-size'))) | 0)) });
    });
}

function auto_aspect_ratio() {
    $('.b-aspect_ratio').each(function(index, jqitem){
        var item_width = $(jqitem).width();
        var item_width_coef = parseFloat($(jqitem).attr('data-aspect-ratio')) * 0.01;
        var item_height = item_width * item_width_coef;
        $(jqitem).css({'height' : Math.floor(item_height) });
    });
}

function index_slider_init() {
    if ($('.b-index_content').length > 0) {
        $('.b-index_content').megaslider({
            'slider_content_class' : 'b-index_slide',
            'slider_content_active_class' : 'b-index_slide_active',

            'slider_button_container_class' : 'b-index_footer_links',
            'slider_button_class' : 'b-index_footer_link',
            'slider_button_active_class' : 'b-index_footer_link_active',

            'pause_time' : 5000,
            'transition_time' : 800,
            'transition_type' : 'margin-shift'
        });
    }
}

function work_slider_init() {
    if ($('.b-work_slider').length > 0) {
        $('.b-work_slider').megaslider({
            'slider_content_class' : 'b-work_slide',
            'slider_content_active_class' : 'b-work_slide_active',

            'slider_button_class' : 'b-work_slider_button',
            'slider_button_active_class' : 'b-work_slider_button_active',

            'slider_arrow_class': 'b-work_slider_arrow',
            'slider_arrow_left_class': 'b-work_slider_arrow_left',
            'slider_arrow_right_class': 'b-work_slider_arrow_right',

            'pause_time' : 5000,
            'transition_time' : 800
        });
    }
}

function app_start() {
    app_started = true;

    // history
    History.Adapter.bind(window, "statechange", history_change_state);
    history_load();
}

/************************************************ MAIN *****************************************************/

function get_work_id(_now) {
    if (_now.indexOf('work') > -1) {
        return parseInt(_now.substr(4));
    }
    return -1;
}

function load_page(_now, callback) {
    if (pagesIdByName(_now) > -1) {
        if (true) {
            var ___now = _now;
            if (get_work_id(_now) > -1) {
                ___now = 'work/id/'+get_work_id(_now);
            }

            $.ajax({
                type : 'GET',
                url : baseUrl + '/api/get_' + ___now,
                timeout : 10000,
                error: function() {
                    if (typeof callback == 'function') { callback(3); }
                },
                success : function(data) {
                    $('#wrap-' + _now).html(data);

                    bg_img_onload();
                    bg_img_update();

                    if (typeof callback == 'function') { callback(1); }
                }
            });
        } else {
            if (typeof callback == 'function') { callback(2); }
        }
    } else {
        if (typeof callback == 'function') { callback(4); }
    }
}

function change_page() {
    var old_now = NOW;

    var state = History.getState();
    NOW = state.data.now;

    if (NOW !== old_now) {
        page_changing = true;

        load_page(NOW, function(success_type)  {
            if (success_type == 3 || success_type == 4) {
                page_changing = false;
            } else {
                var _direction = 'simple';
                if (old_now === 'preloader') {
                    _direction = 'from_start';
                } else if (typeof state.data.direction !== 'undefined' && state.data.direction !== '') {
                    _direction = state.data.direction;
                }

                // main menu update
                main_menu_update();

                // sliders
                index_slider_init();
                work_slider_init();

                switch (_direction) {
                    // ------------------------ only when page currently loaded ---------------------------
                    case 'from_start':
                        $('#wrap-'+NOW+', .b-footer').css({'display' : '', 'opacity' : '0'});
                        bg_on_resize();

                        animateIt('#wrap-'+NOW+', .b-footer', {'opacity' : '1'}, 300, '', 1200, function(){
                            page_changed(old_now, NOW);
                        });
                    break;
                    case 'simple':
                        animateIt('#wrap-'+old_now+', .b-footer', {'opacity' : '0'}, 300, '', 0, function(){
                            $('#wrap-'+old_now).css({'display' : 'none', 'opacity' : ''});
                            $('#wrap-'+NOW).css({'display' : '', 'opacity' : '0'});
                            bg_on_resize();

                            animateIt('#wrap-'+NOW+', .b-footer', {'opacity' : '1'}, 300, '', 1200, function(){
                                page_changed(old_now, NOW);
                            });
                        });
                    break;
                }
            }
        });
    }
}

function page_changed(_old_now, _now) {
    $('#wrap-'+_old_now).css({ 'display' : 'none', 'opacity' : '' }).html('');
    $('#wrap-'+_now).css({ 'opacity' : '' });
    $('.b-footer').css({ 'opacity' : '' });

    page_changing = false;
    bg_on_resize();

    if (_now == 'projects') {
        project_blocks_animate();
    }
}

/************************************************ MISC **********************************************/

function project_blocks_animate() {
    $('.b-projects_block .b-block').each(function(idx, jqitem){
        animateIt($(jqitem), {'-webkit-transform' : 'translate3d(0,0,0)'}, 500, 'ease-out', idx*150);
    });
}















