/*********************************************** MAIN MENU ***************************************************/

function main_menu_update() {
    var page_id = pagesIdByName(NOW);
    if (page_id > -1) {
        grey_menu_hide(function() {
            $('.b-menu_item').removeClass('b-menu_item_selected');

            if (pages[page_id].menu_button !== '') {
                $('.b-menu_item').eq(parseInt(pages[page_id].menu_button)).addClass('b-menu_item_selected');

                grey_menu_show(function(){
                    if (NOW == 'projects') {
                        projects_menu_show();
                    } else if (NOW.indexOf('work') > -1) {
                        work_menu_show();
                    }
                });
            }
        });
    }
}

function grey_menu_show(callback) {
    if ((NOW == 'projects' || NOW.indexOf('work') > -1) && !$('.b-projects_menu').is(':visible')) {
        $('.b-projects_menu').css({'display' : ''});

        animateIt('.b-projects_menu_in', {'height' : 60}, 300, 'ease-out', 250, function(){
            if (typeof callback == 'function') { callback(); }
        });
    } else {
        if (typeof callback == 'function') { callback(); }
    }
}

function grey_menu_hide(callback) {
    if (NOW !== 'projects' && NOW.indexOf('work') == -1 && $('.b-projects_menu').is(':visible')) {
        projects_menu_hide();
        work_menu_hide();

        animateIt('.b-projects_menu_in', {'height' : 0}, 300, 'ease-out', 0, function(){
            $('.b-projects_menu').css({'display' : 'none'});

            if (typeof callback == 'function') { callback(); }
        });
    } else {
        if (typeof callback == 'function') { callback(); }
    }
}

function grey_menu_set_fixed() {
    if (!$('.b-projects_menu_in').hasClass('hdr-fixed')) {
        $('.b-projects_menu_in').addClass('hdr-fixed').css({ 'position' : 'fixed' });
        $('.b-projects_menu').css({ 'height' : $('.b-projects_menu_in').height() });
    }
}

function grey_menu_remove_fixed() {
    if ($('.b-projects_menu_in').hasClass('hdr-fixed')) {
        $('.b-projects_menu_in').removeClass('hdr-fixed').css({ 'position' : '' });
        $('.b-projects_menu').css({ 'height' : '' });
    }
}

/******************************************* PROJECTS MENU ********************************************************/

function projects_menu_show(callback) {
    work_menu_hide(function(){
        $('.b-projects_menu_items').css({'opacity' : '0', 'display' : ''});
        animateIt('.b-projects_menu_items', {'opacity' : 1}, 300, 'ease-out', 0, function(){
            $('.b-projects_menu_items').css({'opacity' : ''});

            if (typeof callback == 'function') { callback(); }
        });
    });
}

function projects_menu_hide(callback) {
    if ($('.b-projects_menu_items').is(':visible')) {
        animateIt('.b-projects_menu_items', {'opacity' : 0}, 300, 'ease-out', 0, function(){
            $('.b-projects_menu_items').css({'opacity' : '', 'display' : 'none'});

            if (typeof callback == 'function') { callback(); }
        });
    } else {
        if (typeof callback == 'function') { callback(); }
    }
}

function projects_menu_update(filter_id) {
    if ($('.b-projects_menu_item_'+filter_id).hasClass('b-projects_menu_item_selected')) {
        if (filter_id != -1) {
            $('.b-projects_menu_item_'+filter_id).removeClass('b-projects_menu_item_selected');

            if ($('.b-projects_menu_item_selected').length == 0) {
                $('.b-projects_menu_item_-1').addClass('b-projects_menu_item_selected');
            }
        }
    } else {
        if (filter_id != -1) {
            $('.b-projects_menu_item_-1').removeClass('b-projects_menu_item_selected');
        } else {
            $('.b-projects_menu_item').removeClass('b-projects_menu_item_selected');
        }
        $('.b-projects_menu_item_'+filter_id).addClass('b-projects_menu_item_selected');
    }
}

/********************************************* WORK MENU ********************************************************/

function work_menu_show(callback) {
    projects_menu_hide(function(){
        $('.b-work_menu_items').css({'opacity' : '0', 'display' : ''});
        animateIt('.b-work_menu_items', {'opacity' : 1}, 300, 'ease-out', 0, function(){
            $('.b-work_menu_items').css({'opacity' : ''});

            if (typeof callback == 'function') { callback(); }
        });
    });
}

function work_menu_hide(callback) {
    if ($('.b-work_menu_items').is(':visible')) {
        animateIt('.b-work_menu_items', {'opacity' : 0}, 300, 'ease-out', 0, function(){
            $('.b-work_menu_items').css({'opacity' : '', 'display' : 'none'});

            if (typeof callback == 'function') { callback(); }
        });
    } else {
        if (typeof callback == 'function') { callback(); }
    }
}

