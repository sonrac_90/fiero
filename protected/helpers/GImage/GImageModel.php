<?php
/**
 * Created by PhpStorm.
 * User: Евгений
 * Date: 09.12.13
 * Time: 20:34
 */

class GImageModel extends CActiveRecord{

    public $image;

    public function rules()
    {
        return array(
            array('image', 'file', 'types'=>'jpg, gif, png'),
        );
    }

} 