<?php

/**
 * Class XML
 */
class XML {

    /**
     * @var $xml DOMDocument
     * @var $root DOMElement
     */
    private  static  $xml;
    private  static  $root;


    public static function init()
    {
        self::$xml = new DOMDocument('1.0','UTF-8');
        self::$root = self::$xml->appendChild(self::$xml->createElement('data'));
        self::$xml->formatOutput = true;
    }


    /**
     * @param $name
     * @param null $parent
     * @param null $text
     * @param bool $CData
     * @return DOMElement
     */
    public static function child($name, $parent = null, $text = null, $CData = false)
    {
        if(!$parent)
            $child = self::$root->appendChild(self::$xml->createElement($name));
        else
            $child = $parent->appendChild(self::$xml->createElement($name));
        if($text !== null)
        {
            if($CData)
            {
                $child->appendChild(self::$xml->createCDATASection($text));
            }
            else
            {
                $child->appendChild(self::$xml->createTextNode($text));
            }
        }
        return $child;
    }

    /**
     * @param bool $return
     * @return string
     */
    public static function out($return = false)
    {
        if($return)
           return self::$xml->saveXML();
        header('Content-type: application/xml');
        echo self::$xml->saveXML();
        exit;
    }

    /**
     * @param string $name
     * @return DOMElement
     */
    public static function getNode($name)
    {
        if($nodes = self::$xml->getElementsByTagName($name))
            return $nodes->item(0);
    }

    public static function getNodeList($name)
    {
        return self::$xml->getElementsByTagName($name);

    }

    /**
     * @param DOMElement $node
     * @param $attribute_name
     * @param $value
     */
    public static function setAttribute($node, $attribute_name, $value)
    {
        $node->setAttribute($attribute_name,$value);
    }

    /**
     * @param DOMElement $node
     * @param $attributes
     */
    public static function setAttributes($node, $attributes)
    {
        if(is_array($attributes))
        {
            foreach($attributes AS $key => $value)
            {
                $node->setAttribute($key, $value);
            }
        }
    }

    /**
     * @param DOMElement $node
     * @param string $attr_name
     * @return mixed
     */
    public static function getAttribute($node,$attr_name)
    {
        return $node->getAttribute($attr_name);
    }

    public static function load($xml)
    {
        self::$xml->loadXML($xml);
    }

    public static function loadArray($arrayNodes, $node = null)
    {
        Array2XML::init();
        $xml = Array2XML::createXML('data',$arrayNodes);
        self::load($xml->saveXML());
        /*if(is_array($arrayNodes))
        {
            foreach($arrayNodes as $key => $value)
            {
                if(is_array($value))
                {
                    if(isset($value[0]) && is_array($value[0]))
                    {
                        $node = self::child($key, $node);
                        return self::loadArray($value, $node);
                    }
                    foreach($value as $k => $v)
                    {
                        if(is_array($v))
                        {
                            $node = self::child($key,$node);
                            return XML::loadArray($v, $node);
                        }
                        else
                        {
                            echo 1;
                            $node = self::child($key,$node);
                            self::setAttribute($node, $k, $v);
                        }

                    }

                }
                else
                {
                    $node = self::child($key, $node, $value);
                }
            }
        }*/
    }

} 