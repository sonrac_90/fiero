<?php
/**
 * Helper для работы с изображениями
 * User: Евгений
 */
/**
 * Class GImage
 * @property string $fileName
 * @property string ext
 */
class GFIleUploader {

    public static $file = null;
    public static $ext = null;
    public static $fileName = null;

    /**
     * @param string $instanceName
     * @return array|CUploadedFile
     */
    public static function uploadImage($instanceName = 'image')
    {
        $model = new GFileModel();
        $model->image = CUploadedFile::getInstanceByName($instanceName);
        self::$ext = $model->image->extensionName;
        self::$fileName = $model->image->name;
        if(!$model->validate())
            return self::$file = $model->errors;
        return self::$file = $model->image;
    }

    /**
     * @param $path
     * @param null $name
     * @param null $ext
     * @return bool
     */
    public static function saveFile($path, $name = null, $ext = null)
    {
        if(!is_object(self::$file))
            return false;
        GHelper::rmkdir($path);
        $name = (isset($name)) ? $name : self::$fileName;
        $ext = (isset($ext)) ? $ext : self::$ext;
        return self::$file->saveAs($path . $name . '.' . $ext);
    }

} 