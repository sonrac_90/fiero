<?php

class SlidersController extends AdminController
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', //Вход, Выход и страницу ошибок показываем всем, включая гостя
                'actions'=>array('login', 'logout', 'error'),
                'users'=>array('*'),
            ),
            array('allow', // Разрешим всё главному админу
                'roles' => array('admin'),
            ),

            array('deny',  // всё остальное всем запрещаем
                'users'=>array('*'),
            ),
        );
    }

	public function actionIndex()
	{
		$this->render('index');
	}

    public function actionChangeIndex(){
        $model = new Sliders();
        $arr = $_POST;
        $cnt = 0;
        for ($i = 0; $i < count($arr['idReal']); $i++){
            $cnt++;
            $attr = $model->findByPk($arr['idReal'][$i]);
            foreach($attr as $k=>$v) $model->$k = $v;
            $model->position = $cnt;
            if ($model->updateByPk($arr['idReal'][$i], $model->attributes)) echo 'OK';
        }
    }

    public function actionDelete($id){
        $model = new Sliders();
        $model->deleteByPk($id);
    }
}