<?php

class DefaultController extends AdminController
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', //Вход, Выход и страницу ошибок показываем всем, включая гостя
                'actions'=>array('login', 'logout', 'error'),
                'users'=>array('*'),
            ),
            array('allow', // Разрешим всё главному админу
                'roles' => array('admin'),
            ),

            array('deny',  // всё остальное всем запрещаем
                'users'=>array('*'),
            ),
        );
    }

	public function actionIndex()
	{
        $model = new Contacts('search');
		$this->render('index', array('model' => $model));
	}

    /**
     * Авторизация в админку
     */
    public function actionLogin()
    {
        $model=new LoginForm;

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            if($model->validate() && $model->login())
            {

                if(str_replace('/', '', Yii::app()->request->baseUrl) == str_replace('/', '', Yii::app()->user->returnUrl))
                {
                    $this->redirect($this->createUrl('projects/index'));
                }
                else
                {
                    $this->redirect(Yii::app()->user->returnUrl);
                }

            }
        }
        // display the login form
        $this->renderPartial('login',array('model'=>$model));
    }

    /**
     * Выход из админки
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect($this->createUrl('default/login'));
    }

    /**
     * Страница обработки ошибок
     */
    public function actionError(){
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionAdd(){
        $model = new ContactSocial();
        if (Yii::app()->request->isPostRequest){
            $model->attributes = $_POST['ContactSocial'];
            if ($model->save()){
                //$this->redirect($this->createUrl('index'));
                echo json_encode($model->attributes);
            }else {
                echo json_encode(array('success' => false));
            }
        }
//        $this->render('update', array('model' => $model));
    }

    public function actionUpdate($id){
        $model = ContactSocial::model()->findByPk($id);

        if (Yii::app()->request->isPostRequest){
            $model->attributes = $_POST['ContactSocial'];
            Contacts::model()->updateByPk(1, $_POST['ContactSocial']);
            if ($model->save()){
                $this->redirect($this->createUrl('index'));
            }
        }

        $this->render('update', array('model' => $model));
    }

    public function actionDelete($id){
        $model = new ContactSocial();
        $model->deleteByPk($id);
        $this->redirect($this->createUrl("index"));
    }

    public function actionChangeIndex(){
        $model = new ContactSocial();
        $arr = $_POST;
        $cnt = 0;
        for ($i = 0; $i < count($arr['idReal']); $i++){
            $cnt++;
            $attr = $model->findByPk($arr['idReal'][$i]);
            foreach($attr as $k=>$v) $model->$k = $v;
            $model->position = $cnt;
            if ($model->updateByPk($arr['idReal'][$i], $model->attributes)) echo 'OK';
        }
    }

    public function actionLoadImage()
    {
        $file = GFileUploader::uploadImage('file');
        if(GFileUploader::validate($file))
        {
            $ext = strtolower(GFileUploader::$ext);
            $rand = time() . rand(0 , 10);
            $dp = GHelper::dynamicPath($rand);
            $basePath = dirname(Yii::app()->basePath);
            $filename = $rand . '.' . $ext;
            if(GFileUploader::saveFile($basePath . About::PATH_TO_IMAGE . '/', $filename))
            {
                GHelper::jsonSuccess(
                    array(
                        'image_url' => Yii::app()->baseUrl . About::PATH_TO_IMAGE . '/' . $filename,
                        'imageName' => $filename
                    )
                );
            }
        }
        GHelper::jsonError('Ошибка сохранения файла');
    }
}