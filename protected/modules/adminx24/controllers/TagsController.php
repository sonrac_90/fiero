<?php

class TagsController extends AdminController
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', //Вход, Выход и страницу ошибок показываем всем, включая гостя
                'actions'=>array('login', 'logout', 'error'),
                'users'=>array('*'),
            ),
            array('allow', // Разрешим всё главному админу
                'roles' => array('admin'),
            ),

            array('deny',  // всё остальное всем запрещаем
                'users'=>array('*'),
            ),
        );
    }

	public function actionIndex()
	{
        $model = new Tags('search');

		$this->render(
            'index',
            array(
                'model' => $model
            )
        );
	}

    public function actionCreate()
    {
        $model = new Tags();
        $model->unsetAttributes();
        if(isset($_POST['Tags']))
        {
            if(Yii::app()->request->isAjaxRequest){
                echo CActiveForm::validate($model);
            }
            $model->attributes = $_POST['Tags'];
            if($model->save()){
                //$this->goBack();
                $this->redirect($this->createUrl('tags/index'));
            }

        }
        $this->render('create', array('model' => $model));
    }

    /**
     * Update tag
     */
    public function actionUpdate($id){
        $model = new Tags();
        if (isset($_POST['Tags'])){
            if (Yii::app()->request->isAjaxRequest){
                echo CActiveForm::validate($model);
            }
            Tags::model()->updateByPk($_GET['id'], $_POST['Tags']);
            $this->redirect($this->createUrl('tags/index'));
        }else{
            $findColumn = $model->findByPk($id);
            foreach($findColumn as $k=>$v) $model->$k = $v;
            $model->isNewRecord = false;
        }
        $this->render('_form', array('model' => $model));
    }

    /**
     * Delete Tag
     */
    public function actionDelete($id){
        $model = new Tags();
        $model->deleteByPk($id);
        $this->redirect($this->createUrl('tags/index'));
    }
}