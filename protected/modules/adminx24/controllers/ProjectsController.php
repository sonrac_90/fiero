<?php

    class ProjectsController extends AdminController {

        /**
         * @return array action filters
         */
        public function filters() {
            return array(
                'accessControl',
                // perform access control for CRUD operations
            );
        }

        /**
         * Specifies the access control rules.
         * This method is used by the 'accessControl' filter.
         *
         * @return array access control rules
         */
        public function accessRules() {
            return array(
                array(
                    'allow',
                    //Вход, Выход и страницу ошибок показываем всем, включая гостя
                    'actions' => array(
                        'login',
                        'logout',
                        'error'
                    ),
                    'users'   => array( '*' ),
                ),
                array(
                    'allow',
                    // Разрешим всё главному админу
                    'roles' => array( 'admin' ),
                ),

                array(
                    'deny',
                    // всё остальное всем запрещаем
                    'users' => array( '*' ),
                ),
            );
        }

        public function actionIndex() {
            //$model = new Projects('search');
            $model = new Projects('search');

            $this->render(
                'index', array(
                    'model' => $model
                )
            );
        }

        public function actionCreate() {
            $model = new Projects();
            $model->unsetAttributes();
            $ext = null;
            $basePath = dirname(Yii::app()->basePath);
            if (isset( $_POST[ 'Projects' ] )) {
                if (Yii::app()->request->isAjaxRequest) {
                    echo CActiveForm::validate($model);
                    exit;
                }
                $model->attributes = $_POST[ 'Projects' ];

                if (isset( $_POST[ 'project_prev_image' ] )) {
                    if (is_file($basePath . $_POST[ 'project_prev_image' ])) {
                        $ext = GFileUploader::pathExtension($basePath . $_POST[ 'project_prev_image' ]);
                        $model->image_ext = $ext;
                    }
                }

                if ($model->validate()) {
                    if ($model->save()) {
                        /*перенос файла изображения в папку проекта*/
                        $filename = $model->primaryKey . '.' . $ext;
                        $dp = GHelper::dynamicPath($model->primaryKey);
                        $image = GImage::openFile($basePath . $_POST[ 'project_prev_image' ]);
                        GImage::imageSave($image, $basePath . Projects::IMAGE_PATH . Projects::IMAGE_PROJECT_PATH . $dp . '/', $filename);

                        $crop = GImage::cropImage(GHelper::$imageSize['preview']['width'], GHelper::$imageSize['preview']['height'], $image);
                        GImage::imageSave($crop, $basePath . Projects::IMAGE_PREVIEW_PATH . Projects::IMAGE_PROJECT_PATH . $dp . '/', $filename);
                        $crop = GImage::cropImage(GHelper::$imageSize['big']['width'], GHelper::$imageSize['big']['height'], $image);
                        GImage::imageSave($crop, $basePath . Projects::IMAGE_BIG_PATH . Projects::IMAGE_PROJECT_PATH . $dp . '/', $filename);
                        $crop = GImage::cropImage(GHelper::$imageSize['small']['width'], GHelper::$imageSize['small']['height'], $image);
                        GImage::imageSave($crop, $basePath . Projects::IMAGE_SMALL_PATH . Projects::IMAGE_PROJECT_PATH . $dp . '/', $filename);
                        /*перенос файла изображения в папку проекта*/

                        if (isset( $_POST[ 'projectTags' ] )) {
                            foreach ($_POST[ 'projectTags' ] as $projectTag) {
                                $tag = Tags::model()->findByPk($projectTag);
                                if ($tag) {
                                    $t = new ProjectTags();
                                    $t->project_id = $model->primaryKey;
                                    $t->tag_id = $projectTag;
                                    $t->save();
                                }
                            }
                        }

                        if (isset( $_POST[ 'creditsName' ] )) {
                            foreach ($_POST[ 'creditsName' ] as $key => $val) {
                                $name = $_POST[ 'creditsName' ][ $key ];
                                $value = $_POST[ 'creditsValue' ][ $key ];
                                $url = $_POST[ 'creditsUrl' ][ $key ];
                                $credit = new ProjectCredits();
                                $credit->name = $name;
                                $credit->value = $value;
                                $credit->url = $url;
                                $credit->position = $key;
                                $credit->project_id = $model->primaryKey;
                                $credit->save();
                            }
                        }

                        if (isset( $_POST[ 'project_images' ] )) {
                            $position = 1;
                            foreach ($_POST[ 'project_images' ] as $projectImage) {
                                $image = ProjectImages::model()->findByPk($projectImage);
                                if ($image) {
                                    $image->project_id = $model->primaryKey;
                                    $image->position = $position;
                                    $image->save();
                                    $position++;
                                }
                            }
                        }
                        $this->redirect($this->createUrl('projects/index'));
                    }
                }
            }
            $this->render(
                'create', array(
                    'model' => $model
                )
            );
        }

        public function actionUpdate($id) {
            $model = Projects::model()->findByPk($id);

            if (isset( $_POST[ 'Projects' ] )) {
                $model->attributes = $_POST[ 'Projects' ];
                if ($model->save()) {
                    /*Обновляем изображения для слайдера*/
                    if (isset( $_POST[ 'project_images' ] )) {
                        $position = 1;
                        foreach ($_POST[ 'project_images' ] as $projectImage) {
                            $image = ProjectImages::model()->findByPk($projectImage);
                            if ($image) {
                                $image->project_id = $model->primaryKey;
                                $image->position = $position;
                                $image->save();
                                $position++;
                            }
                        }
                    }
                    /*Обновляем изображения для слайдера*/

                    /*Обновляем основную картинку*/
                    /*перенос файла изображения в папку проекта*/
                    if (isset( $_POST[ 'project_prev_image' ] )) {
                        $ext = null;
                        $basePath = dirname(Yii::app()->basePath);
                        if (is_file($basePath . $_POST[ 'project_prev_image' ])) {
                            $ext = GFileUploader::pathExtension($basePath . $_POST[ 'project_prev_image' ]);
                            $model->image_ext = $ext;

                            $filename = $model->id . '.' . $ext;
                            $dp = GHelper::dynamicPath($model->id);

                            if (is_file(str_replace('//', '/', $basePath . Projects::IMAGE_PATH . Projects::IMAGE_PROJECT_PATH . $dp . '/' . $model->id . '.' . $model->image_ext)))
                                unlink(str_replace('//', '/', $basePath . Projects::IMAGE_PATH . Projects::IMAGE_PROJECT_PATH . $dp . '/' . $model->id . '.' . $model->image_ext));

                            $image = GImage::openFile($basePath . $_POST[ 'project_prev_image' ]);
                            GImage::imageSave($image, $basePath . Projects::IMAGE_PATH . Projects::IMAGE_PROJECT_PATH . $dp . '/', $filename);

                            $crop = GImage::cropImage(GHelper::$imageSize['preview']['width'], GHelper::$imageSize['preview']['height'], $image);
                            GImage::imageSave($crop, $basePath . Projects::IMAGE_PREVIEW_PATH . Projects::IMAGE_PROJECT_PATH . $dp . '/', $filename);
                            $crop = GImage::cropImage(GHelper::$imageSize['big']['width'], GHelper::$imageSize['big']['height'], $image);
                            GImage::imageSave($crop, $basePath . Projects::IMAGE_BIG_PATH . Projects::IMAGE_PROJECT_PATH . $dp . '/', $filename);
                            $crop = GImage::cropImage(GHelper::$imageSize['small']['width'], GHelper::$imageSize['small']['height'], $image);
                            GImage::imageSave($crop, $basePath . Projects::IMAGE_SMALL_PATH . Projects::IMAGE_PROJECT_PATH . $dp . '/', $filename);

                        }


                    }
                    /*перенос файла изображения в папку проекта*/
                    /*Обновляем основную картинку*/

                    /*Обновляем тэги*/
                    if (isset( $_POST[ 'projectTags' ] )) {
                        $criteria = new CDbCriteria();
                        $criteria->addCondition('project_id = :project_id', 'AND');
                        $criteria->params = array( ':project_id' => $model->id );
                        $criteria->addNotInCondition('tag_id', $_POST[ 'projectTags' ]);
                        ProjectTags::model()->deleteAll($criteria);
                        foreach ($_POST[ 'projectTags' ] as $val) {
                            $tag = ProjectTags::model()->find(
                                'project_id = :project_id AND tag_id = :tag_id', array(
                                    ':project_id' => $model->id,
                                    ':tag_id'     => $val
                                )
                            );
                            if (!$tag) {
                                $tag = new ProjectTags();
                                $tag->project_id = $model->id;
                                $tag->tag_id = $val;
                                $tag->save();
                            }
                        }

                    }
                    /*Обновляем тэги*/

                    /*Обновляем картинки*/
                    if (isset( $_POST[ 'project_images' ] )) {
                        $criteria = new CDbCriteria();
                        $criteria->addCondition('project_id = :project_id', 'AND');
                        $criteria->params = array( ':project_id' => $model->id );
                        $criteria->addNotInCondition('id', $_POST[ 'project_images' ]);
                        ProjectImages::model()->deleteAll($criteria);

                        $position = 1;
                        foreach ($_POST[ 'project_images' ] as $projectImage) {
                            $image = ProjectImages::model()->findByPk($projectImage);
                            if ($image) {
                                $image->project_id = $model->primaryKey;
                                $image->position = $position;
                                $image->save();
                                $position++;
                            }
                        }
                    }
                    /*Обновляем картинки*/

                    /*Обновляем кредитсы*/
                    if (isset( $_POST[ 'creditsName' ] )) {
                        if (isset( $_POST[ 'creditsID' ] )) {
                            $criteria = new CDbCriteria();
                            $criteria->addCondition('project_id = :project_id', 'AND');
                            $criteria->params = array( ':project_id' => $model->id );
                            $criteria->addNotInCondition('id', $_POST[ 'creditsID' ]);
                            ProjectCredits::model()->deleteAll($criteria);
                        }
                        $position = 0;
                        foreach ($_POST[ 'creditsName' ] as $key => $val) {
                            $position++;

                            if (isset( $_POST[ 'creditsID' ][ $key ] )) {
                                $criteria = new CDbCriteria();
                                $criteria->addCondition('id = :id');
                                $criteria->addCondition('project_id = :project_id');
                                $criteria->params = array(
                                    ':id' => $_POST[ 'creditsID' ][ $key ],
                                    ':project_id' => $model->id
                                );

                                $credit = ProjectCredits::model()->find($criteria);
                                if ($credit) {
                                    $credit->name = $_POST[ 'creditsName' ][ $key ];
                                    $credit->value = $_POST[ 'creditsValue' ][ $key ];
//                                    $credit->url = $_POST[ 'creditsUrl' ][ $key ];
                                    $credit->position = $position;
                                    $credit->save();

                                }
                            } else {
                                $credit = new ProjectCredits();
                                $credit->name = $_POST[ 'creditsName' ][ $key ];
                                $credit->value = $_POST[ 'creditsValue' ][ $key ];
//                                $credit->url = $_POST[ 'creditsUrl' ][ $key ];
                                $credit->position = $position;
                                $credit->project_id = $model->id;
                                $credit->save();
                            }
                        }
                    }
                    /*Обновляем кредитсы*/

                    //$this->redirect($this->createUrl('projects/index'));
                }

                if (isset($_POST['Description'])) {
                    $count = 0;
                    $description = $_POST['Description'];
                    foreach ($description as $_description) {
                        $id = isset($_description['id']) ? $_description['id'] : null;
                        unset($_description['id']);

                        $photo = null;
                        if (isset($_description['DescriptionPhotos'])) {
                            $photo = $_description['DescriptionPhotos'];
                        }

                        /** @var $descriptionModel Description */
                        $descriptionModel = new Description();
                        if (!is_null($id)) {
                            $descriptionModel = Description::model()->findByPk(intval($id));
                        }

                        $descriptionModel->attributes = $_description;
                        $descriptionModel->project_id = $model->getPrimaryKey();

                        if (is_null($descriptionModel->position)) {
                            $count++;
                            $descriptionModel->position = $count;
                        }

                        if ($descriptionModel->validate()) {
                            if ( intval($id) > 0) {
                                $descriptionModel->update();
                            } else {
                                $descriptionModel->save();
                            }
                        }

                        if (is_array($photo)) {
                            $position = 0;
                            foreach ($photo as $_photo) {
                                $id = intval($_photo['id']);
                                unset($_photo['id']);
                                /** @var DescriptionPhoto $photoModel */
                                $photoModel = DescriptionPhoto::model()->findByPk($id);

                                $photoModel->attributes = $_photo;
                                $photoModel->position = $position;
                                $position++;
                                $photoModel->ext = pathinfo($photoModel->ext, PATHINFO_EXTENSION);
                                $photoModel->description_id = $descriptionModel->getPrimaryKey();

                                if ($photoModel->validate()) {
                                    $photoModel->update();
                                }
                            }
                        }

                    }
                }
            }

            $this->render(
                'update', array(
                    'model' => $model
                )
            );
        }

        /**
         * Delete Tag
         */
        public function actionDelete($id) {
            $model = Projects::model()->findByPk($id);
            if ($model) {
                $model->delete();
            }
            $this->redirect($this->createUrl('projects/index'));
        }

        public function actionDeletePhotoDescription ($id) {
            DescriptionPhoto::model()->deleteByPk($id);
        }

        public function actionUploadGallery() {
            $file = GFileUploader::uploadImage('ImageGallery');
            if (GFileUploader::validate($file)) {
                $ext = strtolower(GFileUploader::$ext);
                $model = new ProjectImages();
                $model->ext = $ext;
                if ($model->save()) {
                    $dp = GHelper::dynamicPath($model->primaryKey);
                    $basePath = dirname(Yii::app()->basePath);
                    $filename = $model->primaryKey . '.' . $ext;
                    if (GFileUploader::saveFile($basePath . Projects::IMAGE_PATH . $dp . '/', $filename)) {
                        $image = GImage::openFile($basePath . Projects::IMAGE_PATH . $dp . '/' . $filename);

                        $crop = GImage::cropImage(GHelper::$imageSize['preview']['width'], GHelper::$imageSize['preview']['height'], $image);
                        GImage::imageSave($crop, $basePath . Projects::IMAGE_PREVIEW_PATH . $dp . '/', $filename);

                        $crop = GImage::cropImage(GHelper::$imageSize['big']['width'], GHelper::$imageSize['big']['height'], $image);
                        GImage::imageSave($crop, $basePath . Projects::IMAGE_BIG_PATH . $dp . '/', $filename);

                        $crop = GImage::cropImage(GHelper::$imageSize['small']['width'], GHelper::$imageSize['small']['height'], $image);
                        GImage::imageSave($crop, $basePath . Projects::IMAGE_SMALL_PATH . $dp . '/', $filename);

                        GHelper::jsonSuccess(
                            array(
                                'image_url' => Yii::app()->baseUrl . Projects::IMAGE_PREVIEW_PATH . $dp . '/' . $filename,
                                'image_id'  => $model->primaryKey,
                            )
                        );
                    }
                    $model->delete();
                }
            }
            GHelper::jsonError('Ошибка сохранения файла');
        }

        public function actionUploadGalleryDescription() {
            $file = GFileUploader::uploadImage('ImageGallery');
            if (GFileUploader::validate($file)) {
                $ext = strtolower(GFileUploader::$ext);
                $model = new DescriptionPhoto();
                $model->ext = $ext;
                if ($model->save()) {
                    $dp = GHelper::dynamicPath($model->primaryKey);
                    $basePath = dirname(Yii::app()->basePath);
                    $filename = $model->primaryKey . '_description.' . $ext;
                    if (GFileUploader::saveFile($basePath . Projects::IMAGE_PATH . $dp . '/', $filename)) {
                        $image = GImage::openFile($basePath . Projects::IMAGE_PATH . $dp . '/' . $filename);

                        $crop = GImage::cropImage(GHelper::$imageSize['preview']['width'], GHelper::$imageSize['preview']['height'], $image);
                        GImage::imageSave($crop, $basePath . Projects::IMAGE_PREVIEW_PATH . $dp . '/', $filename);

                        $crop = GImage::cropImage(GHelper::$imageSize['big']['width'], GHelper::$imageSize['big']['height'], $image);
                        GImage::imageSave($crop, $basePath . Projects::IMAGE_BIG_PATH . $dp . '/', $filename);

                        $crop = GImage::cropImage(GHelper::$imageSize['small']['width'], GHelper::$imageSize['small']['height'], $image);
                        GImage::imageSave($crop, $basePath . Projects::IMAGE_SMALL_PATH . $dp . '/', $filename);

                        GHelper::jsonSuccess(
                            array(
                                'image_url' => Yii::app()->baseUrl . Projects::IMAGE_PREVIEW_PATH . $dp . '/' . $filename,
                                'image_id'  => $model->primaryKey,
                            )
                        );
                    }
                    $model->delete();
                }
            }
            GHelper::jsonError('Ошибка сохранения файла');
        }

        public function actionUploadImageText() {
            //$file = GFIleUploader::uploadImage('ImageText');
            echo 'as';
        }

        public function actionUploadPreview() {
            $file = GFileUploader::uploadImage('imagePreview');
            if (GFileUploader::validate($file)) {
                $ext = strtolower(GFileUploader::$ext);
                $rand = time() . rand(0, 10);
                $dp = GHelper::dynamicPath($rand);
                $basePath = dirname(Yii::app()->basePath);
                $filename = $rand . '.' . $ext;
                if (GFileUploader::saveFile($basePath . Projects::IMAGE_TEMP_PATH . $dp . '/', $filename)) {
                    $image = GImage::openFile($basePath . Projects::IMAGE_TEMP_PATH . $dp . '/' . $filename);

                    $crop = GImage::cropImage(GHelper::$imageSize['preview']['width'], GHelper::$imageSize['preview']['height'], $image);
                    GImage::imageSave($crop, $basePath . Projects::IMAGE_TEMP_PREVIEW_PATH . $dp . '/', $filename);
                    GImage::imageSave($image, $basePath . Projects::IMAGE_PATH . $dp . '/', $filename);

                    GHelper::jsonSuccess(
                        array(
                            'image_url' => Yii::app()->baseUrl . Projects::IMAGE_TEMP_PREVIEW_PATH . $dp . '/' . $filename,
                            'original'  => Projects::IMAGE_TEMP_PATH . $dp . '/' . $filename
                        )
                    );
                }
            }
            GHelper::jsonError('Ошибка сохранения файла');
        }

        /**
         * Upload Image
         */
        public function actionUploadImage() {
            if (!isset( $_GET[ 'qqfile' ] )) {
                $this->render('upload_image', array());
            } else {

                Yii::import("ext.EAjaxUpload.qqFileUploader");
                $folder = Yii::app()->getBasePath() . "/../uploads/images/content/";
                if (is_writable($folder)) {
                    $allowedExtensions = array( "jpg" );
                    $sizeLimit = 2 * 1024 * 1024;
                    $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
                    $result = $uploader->handleUpload($folder);

                    if (!isset( $result[ 'filename' ] )) {
                        echo 'Error';
                    } else {
                        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
                    }
                }
            }
        }

        public function actionDeleteCredits ($id) {
            ProjectCredits::model()->deleteByPk($id);
        }

        public function actionDeleteDescription ($id, $parent=null) {
            $description = Description::model()->findByPk($id);
            if ($description && $parent) {
                Description::model()->deleteAllByAttributes(array('position' => $description->position));
            } else {
                Description::model()->deleteByPk($id);
            }
        }
    }