<?php

class ContactsController extends AdminController {

    public function actionIndex(){
        $model = new Contacts();
        $this->render('index', array('model' => $model));
    }

    public function actionUpdate(){
        $model = new Contacts('search');
        if (isset($_POST['Contacts'])){
            if (Yii::app()->request->isAjaxRequest){
                echo CActiveForm::validate($model);
            }
            Contacts::model()->updateByPk($_GET['id'], $_POST['Contacts']);
            $this->redirect($this->createUrl('contacts/index'));
        }
        $fnd = $model->findByPk($_GET['id']);
        foreach ($fnd as $key=>$val) $model->$key = $val;

        $this->render('update', array('model' => $model));
    }

    public function actionLoadImage()
    {
        $file = GFileUploader::uploadImage('file');
        if(GFileUploader::validate($file))
        {
            $ext = strtolower(GFileUploader::$ext);
            $rand = time() . rand(0 , 10);
            $dp = GHelper::dynamicPath($rand);
            $basePath = dirname(Yii::app()->basePath);
            $filename = $rand . '.' . $ext;
            if(GFileUploader::saveFile($basePath . About::PATH_TO_IMAGE . '/', $filename))
            {
                GHelper::jsonSuccess(
                    array(
                        'image_url' => Yii::app()->baseUrl . About::PATH_TO_IMAGE . '/' . $filename,
                        'imageName' => $filename
                    )
                );
            }
        }
        GHelper::jsonError('Ошибка сохранения файла');
    }
}