
<?php

class AboutController extends AdminController
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionUpdate($id){
        $model = new About('search');
        if (isset($_POST['About'])){

            if (Yii::app()->request->isAjaxRequest){
                echo CActiveForm::validate($model);
            }
            About::model()->updateByPk($_GET['id'], $_POST['About']);
            $this->redirect($this->createUrl('about/index'));
        }
        $AboutInfo = $model->findByPk($id);
        $model->attributes = $AboutInfo->attributes;
        $this->render('update', array('model' => $model));
    }

    public function actionLoadImage()
    {
        $file = GFileUploader::uploadImage('file');
        if(GFileUploader::validate($file))
        {
            $ext = strtolower(GFileUploader::$ext);
            $rand = time() . rand(0 , 10);
            $dp = GHelper::dynamicPath($rand);
            $basePath = dirname(Yii::app()->basePath);
            $filename = $rand . '.' . $ext;
            if(GFileUploader::saveFile($basePath . About::PATH_TO_IMAGE . '/', $filename))
            {
                GHelper::jsonSuccess(
                    array(
                        'image_url' => Yii::app()->baseUrl . About::PATH_TO_IMAGE . '/' . $filename,
                        'imageName' => $filename
                    )
                );
            }
        }
        GHelper::jsonError('Ошибка сохранения файла');
    }
}