<?php
/* @var $this AboutController */

$this->breadcrumbs=array(
    $this->module->id
);

GHelper::registerJS(
    array(
        'tinymce/tinymce.min.js', //Подключаем текстовый редактор
        'admin/project_tinymce_init.js', //Подключаем текстовый редактор
        'admin/project_credits.js', //Подключаем текстовый редактор

        'bootstrap-tokenfield.js', //Подключаем плагин для формирования тэго
        'admin/project_tags.js', //Подключаем склипт для инициализации тэгов

        'plupload/moxie.min.js', //Подключаем текстовый редактор
        'plupload/plupload.min.js', //Подключаем текстовый редактор
        'admin/project_plupload_images.js', //Подключаем текстовый редактор
        'admin/aboutImage.js', //Подключаем текстовый редактор
    ),CClientScript::POS_END
);

$form = $this->beginWidget('CActiveForm', array( 'id' => 'formAbout[uploadImage]'

));
$ext = GFileUploader::pathExtension($model->imgLink);
$fid = GFileUploader::pathFilename($model->imgLink);
$fname =  $fid . '.' . $ext;

$imgLink = Yii::app()->baseUrl . About::PATH_TO_IMAGE . "/" . $fname;
?>
<div class="contact_form">
    <div class="phone" >
        <div><?php echo $form->label($model, 'text');?></div>
        <div><?php echo $form->textArea($model, 'text', array('class' => 'input_tinymce')); ?></div>
        <div><?php echo $form->error($model, 'text');?></div>
    </div>
    <div class="about_image">
        <div><?php echo $form->label($model, 'imgLink');?></div>
        <div><?php echo $form->hiddenField($model, 'imgLink', array('class' => 'form-control'));?></div>
        <div><?php echo CHtml::image($imgLink, 'Изображение', array('width' => '200', 'height' => '200'))?></div>
        <div id="" style="text-align: center; overflow: hidden; width: 150px; height: 50px; border: 1px solid #038f1a; background-color: #038f1a;">
            <div style="color: #fff; font-size: 20px; font-weight: bold;">Выбрать файл</div>
            <?php echo CHtml::fileField('file', $fname, array('id' => 'fileAbout', 'name' => 'uploadImage', 'style' => 'margin-top: -50px; margin-left:-410px; -moz-opacity: 0; filter: alpha(opacity=0); opacity: 0; font-size: 150px; height: 100px;'))?>
        </div>
<!--        <div id="fileAbout"></div>-->
        <div><?php echo $form->error($model, $fname);?></div>
    </div>
    <?php
    echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-success'));
    $this->endWidget();
    ?>
</div>