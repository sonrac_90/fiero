<?php
$this->title = 'О нас';
$model = new About();
?>

    <div class="about">
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'contacts-list',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'itemsCssClass' => 'table table-striped',
        'enableHistory' => true,
        'columns' => array(
            array(
                'name' => 'imgLink',
                'type' => 'html',
                'value' => function ($data) {return "<img src=\"" . Yii::app()->baseUrl. About::PATH_TO_IMAGE . "/" . "$data->imgLink\" width=100 height=100 />"; },
                'filter' => false
            ),
            array(
                'name' => 'text',
                'type' => 'html',
                'filter' => false,
            ),
            array( //Кнопочки
                'class'=>'CButtonColumn',
                'template'=>'{update}',
                'buttons'=>array
                (
                    'update' => array
                    (
                        'label'=>'Изменить',
                        'imageUrl'=>null,
                        'options'=>array('class'=>'glyphicon glyphicon-pencil'),
                        'visible' => 'Yii::app()->user->checkAccess("admin")',
                    )
                )
            )
        )
    ));