<?php

    GHelper::registerJS(
        array(
            'tinymce/tinymce.min.js',
            //Подключаем текстовый редактор
            'admin/project_tinymce_init.js',
            //Подключаем текстовый редактор
            'admin/project_credits.js',
            //Подключаем текстовый редактор

            'bootstrap-tokenfield.js',
            //Подключаем плагин для формирования тэго
            'admin/project_tags.js',
            //Подключаем склипт для инициализации тэгов

            'plupload/moxie.min.js',
            //Подключаем текстовый редактор
            'plupload/plupload.min.js',
            //Подключаем текстовый редактор
            'admin/project_plupload_images.js',
            'admin/jquery.pekeUpload2.js',
            'admin/pekeUploadEvent.js',
            //Подключаем текстовый редактор
        ), CClientScript::POS_END
    );

    GHelper::registerCSS(
        array(
            'bootstrap-tokenfield.css',
            'tokenfield-typeahead.css',
        )
    );

    /**
     * @var $this  AdminController
     * @var $model Tags
     * @var $form  CActiveForm
     */

    $form = $this->beginWidget(
        'CActiveForm', array(
            'id'                     => 'project-form',
            //'enableAjaxValidation'   => true,
            'enableClientValidation' => true,
            'htmlOptions'            => array( 'enctype' => 'multipart/form-data' ),
            'focus'                  => array(
                $model,
                'name'
            ),
            //Перевести фокус сразу на имя проекта
        )
    );

    $this->sideBar = <<<SIDEBAR
<div class="credits-block">
    <div class="inputs">
        <div>
            <span>название</span>
            <input type="text" class="form-control"/>
        </div>
        <div>
            <span>значение</span>
            <input type="text" class="form-control"/>
        </div>
        <div>
            <span>ссылка</span>
            <input type="text" class="form-control"/>
        </div>
    </div>
    <div class="links">
        <span>Добавить</span>
    </div>
</div>
SIDEBAR;

?>
    <div class = "col-xs-12 col-sm-9">

        <?php echo $form->errorSummary($model); ?>
        <div class = "tag_form">
            <div class = "name">
                <div><?php echo $form->label($model, 'name'); ?></div>
                <div><?php echo $form->textField($model, 'name', array( 'class' => 'form-control' )); ?></div>
                <div><?php echo $form->error($model, 'name'); ?></div>
            </div>
            <div class = "title">
                <div><?php echo $form->label($model, 'text'); ?></div>
                <div><?php echo $form->textArea($model, 'text', array( 'class' => 'form-control input_tinymce' )); ?></div>
                <div><?php echo $form->error($model, 'text') ?></div>
            </div>
            <div class = "client">
                <div><?php echo $form->label($model, 'client'); ?></div>
                <div><?php echo $form->textField($model, 'client', array( 'class' => 'form-control input_tinymce' )); ?></div>
                <div><?php echo $form->error($model, 'client') ?></div>
            </div>
            <div class = "agency">
                <div><?php echo $form->label($model, 'agency'); ?></div>
                <div><?php echo $form->textField($model, 'agency', array( 'class' => 'form-control input_tinymce' )); ?></div>
                <div><?php echo $form->error($model, 'agency') ?></div>
            </div>
            <div class = "industry">
                <div><?php echo $form->label($model, 'industry'); ?></div>
                <div><?php echo $form->textField($model, 'industry', array( 'class' => 'form-control input_tinymce' )); ?></div>
                <div><?php echo $form->error($model, 'industry') ?></div>
            </div>
            <div class = "short_name"><hr/>
                <?php
                    /**
                     * @var int         $key
                     * @var Description $value
                     */
                    $numberLast = null;
                    foreach ($model->descriptions as $key => $value) {
                        $end = false;
                        if ($numberLast != $value->position) {
                            if (!is_null($numberLast)) {
                                echo '<hr/>
                                    <fieldset><button class = "btn btn-success addText">
                                        Добавить текстовое поле
                                    </button></fieldset>
                                 ';
                                echo '</filedset>';
                                echo "<hr/>";
                                $end = true;
                            }

                            $numberLast = $value->position;
                            echo "<fieldset>";
                            echo '<legend>
                                      Описание ' . intval($key + 1) . '
                                      <i class="glyphicon glyphicon-remove deleteDescription" data = "' . $value->id . '" data-parent="parent"></i>
                                  </legend>';
                        }

                        echo '<div class="descriptionBlock"> Блок ' . $value->number;
                ?>
                        <i class="glyphicon glyphicon-remove deleteDescription" data="<?= $value->id ?>" ></i>
                        <div>
                            <?php echo $form->hiddenField($value, 'id',
                                                          array(
                                                              'name' => 'Description[' . $key . ']' . '[id]'
                                                          )
                            );
                            ?>
                        </div>
                        <div>
                            <?php
                                echo $form->textField($value, 'title',
                                                      array(
                                                          'class' => 'form-control',
                                                          'name'  => 'Description[' . $key . '][title]',
                                                      )
                                );
                            ?>
                        </div>
                        <div>
                            <?php  echo $form->error($value, 'title'); ?>
                        </div>
                        <div><?php echo $form->label($model, 'text' ); ?></div>
                        <div class = "text">
                            <div>
                                <?php
                                    echo $form->textArea($value, 'text',
                                                         array(
                                                             'class' => 'input_tinymce form-control',
                                                             'id' => 'selector_' . $key,
                                                             'name' => 'Description[' . $key . '][text]',
                                                         )
                                    );
                                    echo $form->hiddenField($value, 'number',
                                                            array(
                                                                'name' => 'Description['.$key.'][number]',                                                            )
                                    );
                                    echo $form->hiddenField($value, 'position_child',
                                                            array(
                                                                'name' => 'Description['.$key.'][position_child]',                                                            )
                                    );
                                    echo $form->hiddenField($value, 'position',
                                                            array(
                                                                'name' => 'Description['.$key.'][position_child]',                                                            )
                                    );
                                ?>
                            </div>
                            <div>
                                <?php echo $form->error($value, 'title'); ?>
                            </div>
                        </div>
                        <div><br/>
                            <?php
                                echo "<div data='{$value->id}' data-key=" . $key . " class=\"images\">";
                                if ($value->descriptionPhotos) {
                                    foreach ($value->descriptionPhotos as $numPhoto => $_photo) {
                                        echo "<div class='sortDiv'>";
                                        $imagePath = GHelper::getFullImagePath($_photo, 2, true);
                                        echo $form->hiddenField($_photo, 'ext', array( 'name' => 'Description[' . $key . '][DescriptionPhotos][' . $numPhoto . '][ext]', 'value' => $imagePath));
                                        echo $form->hiddenField($_photo, 'id', array( 'name' => 'Description[' . $key . '][DescriptionPhotos][' . $numPhoto . '][id]'));
                                        echo '<img src="' . $imagePath . "\" /><br/>";
                                        echo '<div class = "btn btn-danger deletePhoto" data = "' . $_photo->id . '">Удалить</div>';
                                        echo '</div>';
                                    }
                                }
                                echo "</div>";
                            ?>
                        </div><br/>
                        <div class="clearFix"></div>
                        <input type="file" class = 'fileUpload' value="Добавить изображение" />
                    <?php
                        echo "</div>";
                        if ($key == (sizeof($model->descriptions) - 1)) {
                            echo '<hr/>
                                    <fieldset><button class = "btn btn-success addText">
                                        Добавить текстовое поле
                                    </button></fieldset>
                                 ';
                            echo '</filedset>';
                            echo "<hr/>";
                        }
                    ?>
                <?php } ?>
                </fieldset>
                <button id="addDescription" class = "btn btn-success" >
                    Добавить описание
                </button>
            </div>
            <hr />
            <div class = "tags">
                <div class = "tagList">
                    <div class = "items">
                        <?php

                            if ($model->isNewRecord) {
                                if (isset( $_POST[ 'projectTags' ] )) {
                                    $criteria = new CDbCriteria();
                                    $criteria->addInCondition('id', $_POST[ 'projectTags' ]);
                                    $tags = Tags::model()->findAll($criteria);
                                    if ($tags) {
                                        foreach ($tags as $tag) {
                                            echo "<label class='label label-primary'>";
                                            echo $tag->name;
                                            echo CHtml::hiddenField('projectTags[]', $tag->id);
                                            echo '<i class="glyphicon glyphicon-remove"></i>';
                                            echo "</label>";
                                        }
                                    }
                                }
                            } else {
                                if (isset( $model->projectTags )) {
                                    foreach ($model->projectTags as $tag) {
                                        echo "<label class='label label-primary'>";
                                        echo $tag->tag->name;
                                        echo CHtml::hiddenField('projectTags[]', $tag->tag->id);
                                        echo '<i class="glyphicon glyphicon-remove"></i>';
                                        echo "</label>";
                                    }
                                }
                            }
                        ?>
                    </div>
                    <?php
                        echo CHtml::textField('input_tags', '', array( 'class' => 'tags_input' ));
                    ?>
                </div>
                <?php
                    echo CHtml::dropDownList('tags', null, Tags::tagList());
                ?>
            </div>
            <HR />
            <div class = "project_image">
                <div>Превьюшка</div>
                <div class = "image_preview">
                    <?php

                        if ($model->isNewRecord) {
                            if (isset( $_POST[ 'project_prev_image' ] )) {
                                $ext = GFileUploader::pathExtension($_POST[ 'project_prev_image' ]);
                                $fid = GFileUploader::pathFilename($_POST[ 'project_prev_image' ]);
                                $fname = $fid . '.' . $ext;
                                $dp = GHelper::dynamicPath($fid);
                                $prevUrl = Yii::app()->baseUrl . Projects::IMAGE_TEMP_PREVIEW_PATH . $dp . '/' . $fname;
                                echo "<div class='prev_image'>";
                                echo CHtml::image(
                                    $prevUrl, 'Превьюшка', array(
                                        'width'  => '109px',
                                        'height' => '77px'
                                    )
                                );
                                echo CHtml::hiddenField('project_prev_image', $_POST[ 'project_prev_image' ]);
                                echo "</div>";
                            }
                        } else {
                            if (isset( $model->image_ext )) {
                                $fname = $model->id . '.' . $model->image_ext;
                                $dp = GHelper::dynamicPath($model->id);
                                $prevUrl = Yii::app()->baseUrl . Projects::IMAGE_PREVIEW_PATH . Projects::IMAGE_PROJECT_PATH . $dp . '/' . $fname;
                                echo "<div class='prev_image'>";
                                echo CHtml::image(
                                    $prevUrl, 'Превьюшка', array(
                                        'width'  => '109px',
                                        'height' => '77px'
                                    )
                                );
                                echo "</div>";
                            }
                        }
                    ?>
                </div>
                <div id = "project_image_container">
                    <div id = "projectBrowseFile"><img src = "<?= Yii::app()->baseUrl ?>/media/images/add_image.png"
                                                       alt = "Добавить слайдер" /></div>
                </div>
            </div>
            <hr />
            <div class = "project_images">
<!--                <div>Слайдер</div>-->
<!--                <div class = "images">-->
<!--                    --><?php
//                        if ($model->isNewRecord) {
//                            if (isset( $_POST[ 'project_images' ] )) {
//                                $criteria = new CDbCriteria();
//                                $criteria->addInCondition('id', $_POST[ 'project_images' ]);
//                                $criteria->order = 'position ASC';
//                                $images = ProjectImages::model()->findAll($criteria);
//                                if ($images) {
//                                    foreach ($images as $image) {
//
//                                        $ext = $image->ext;;
//                                        $fid = $image->id;
//                                        $fname = $fid . '.' . $ext;
//                                        $dp = GHelper::dynamicPath($fid);
//                                        $prevUrl = Yii::app()->baseUrl . Projects::IMAGE_PREVIEW_PATH . $dp . '/' . $fname;
//
//                                        echo "<div class='project_image'>";
//                                        echo CHtml::image(
//                                            $prevUrl, 'Превьюшка', array(
//                                                'width'  => '109px',
//                                                'height' => '77px'
//                                            )
//                                        );
//                                        echo CHtml::hiddenField('project_images[]', $image->id);
//                                        echo "</div>";
//                                    }
//                                }
//
//                            }
//                        } else {
//                            if (isset( $model->images )) {
//                                foreach ($model->images as $image) {
//                                    $ext = $image->ext;;
//                                    $fid = $image->id;
//                                    $fname = $fid . '.' . $ext;
//                                    $dp = GHelper::dynamicPath($fid);
//                                    $prevUrl = Yii::app()->baseUrl . Projects::IMAGE_PREVIEW_PATH . $dp . '/' . $fname;
//
//                                    echo "<div class='project_image'>";
//                                    echo CHtml::image(
//                                        $prevUrl, 'Превьюшка', array(
//                                            'width'  => '109px',
//                                            'height' => '77px'
//                                        )
//                                    );
//                                    echo CHtml::hiddenField('project_images[]', $image->id);
//                                    echo "</div>";
//                                }
//                            }
//                        }
//                    ?>
<!--                </div>-->
<!--                <div id = "images_container">-->
<!--                    <div id = "imagesBrowseFile"><img src = "--><?//= Yii::app()->baseUrl ?><!--/media/images/add_image.png"-->
<!--                                                      alt = "Добавить слайдер" /></div>-->
<!--                </div>-->
            </div>
        </div>

        <?php
            echo CHtml::submitButton(( $model->isNewRecord ) ? 'Добавить' : 'Сохранить', array( 'class' => 'btn btn-success' ));
        ?>

    </div>

    <div class = "col-xs-6 col-sm-3 sidebar-offcanvas" role = "navigation">
        <span>Сотрудники</span>
        <hr />
        <div class = "credits-blocks">
            <?php

                if ($model->isNewRecord) {
                    if (isset( $_POST[ 'creditsName' ] )) {
                        foreach ($_POST[ 'creditsName' ] as $key => $val) {
                            ?>
                            <div class = "credits-block">
                                <i class = "glyphicon glyphicon-remove" ></i>
                                <div class = "inputs">
                                    <div>
                                        <span>Должность</span>
                                        <input type = "text" name = "creditsName[<?= $key ?>]"
                                               value = "<?= $_POST[ 'creditsName' ][ $key ] ?>"
                                               class = "form-control" />
                                    </div>
                                    <div>
                                        <span>Имя, фамилия</span>
                                        <input type = "text" name = "creditsValue[<?= $key ?>]"
                                               value = "<?= $_POST[ 'creditsValue' ][ $key ] ?>"
                                               class = "form-control" />
                                    </div>
                                    <div>
<!--                                        <span>ссылка</span>-->
<!--                                        <input type = "text" name = "creditsUrl[--><?//= $key ?><!--]"-->
<!--                                               value = "--><?//= $_POST[ 'creditsUrl' ][ $key ] ?><!--" class = "form-control" />-->
<!--                                    </div>-->
                                </div>
                            </div>
                        <?php
                        }
                    }
                } else {
                    if (isset( $model->projectCredits )) {
                        foreach ($model->projectCredits as $credit) {
                            ?>
                            <div class = "credits-block">
                                <i class = "glyphicon glyphicon-remove" data = "<?= $credit->id ?>"></i>
                                <div class = "inputs">
                                    <div>
                                        <span>Название</span>
                                        <input type = "text" name = "creditsName[]" value = "<?= $credit->name ?>"
                                               class = "form-control" />
                                    </div>
                                    <div>
                                        <span>Значение</span>
                                        <input type = "text" name = "creditsValue[]" value = "<?= $credit->value ?>"
                                               class = "form-control" />
                                    </div>
<!--                                    <div>-->
<!--                                        <span>Ссылка</span>-->
<!--                                        <input type = "text" name = "creditsUrl[]" value = "--><?//= $credit->url ?><!--"-->
<!--                                               class = "form-control" />-->
<!--                                    </div>-->
                                    <input type = "hidden" name = "creditsID[]" value = "<?= $credit->id ?>" />
                                </div>
                            </div>
                        <?php
                        }
                    }
                }
            ?>
        </div>
        <hr />
        <div class = "links">
            <div id = "addCredit" class = "btn btn-success">Добавить сотрудника</div>
        </div>
    </div>

<?php

    $this->endWidget();

?>