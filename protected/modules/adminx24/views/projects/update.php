<?php

    /**
     * @var $this AdminController
     * @var $model Projects
     */

    Yii::app()->clientScript->registerScript('script', <<<JS
        var projectId = $model->id;
JS
        , CClientScript::POS_HEAD);

    $this->title = 'Редактирование проекта';
    $this->renderPartial('_form', array('model' => $model));