<?php

/**
 * @var $this AdminController
 * @var $model Projects
 */

$this->title = 'Проекты';

?>

<div class="tags">
    <?php

    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'tags-list',
        'dataProvider' => $model->search(),
        //'filter' => $model,
        //'enableHistory' => true,
        'itemsCssClass' => 'table table-striped',
        //'template' => '{items}{pager}',

        'pager'         => array(
            'header'         => '',
            'prevPageLabel'  => '<',
            'nextPageLabel'  => '>',
            'lastPageLabel'  => '>>',
            'firstPageLabel' => '<<',
            'hiddenPageCssClass' => '',
            'htmlOptions'    => array(
                'class' => 'pagination pagination-centered',
            ),
            'selectedPageCssClass' => 'active',
        ),
        'pagerCssClass' => '',

        'columns' => array(
            array(
                'name' => 'name',
                'filter' => false,
            ),
            array(
                'name' => 'short_name',
                'filter' => false,
            ),
            'client',
            'agency',
            'industry',
            array(
                'header' => 'Тэги',
                'type' => 'html',
                'value' => function($data) {
                        $tags = ProjectTags::tagsByProject($data->id);
                        $tagList = '';
                        if($tags)
                        {
                            foreach($tags as $tag)
                            {
                                $tagList .= '<span>' . $tag->tag->name . '</span> ';
                            }
                        }
                        return $tagList;
                    },
                'filter' => false,
            ),
            array(
                'name' => 'visible',
                'type' => 'html',
                'value' => function($data) {
                        $class = ($data->visible) ? 'glyphicon-eye-open' : 'glyphicon-eye-close';
                        return "<span class='glyphicon {$class}'></span>";
                    },
                'filter' => false,
            ),
            array( //Кнопочки
                'class'=>'CButtonColumn',
                'template'=>'{update}{delete}',
                'buttons'=>array
                (
                    'update' => array
                    (
                        'label'=>'Изменить',
                        'imageUrl'=>null,
                        'options'=>array('class'=>'glyphicon glyphicon-pencil'),
                        'visible' => 'Yii::app()->user->checkAccess("admin")',
                    ),
                    'delete' => array
                    (
                        'label'=>'Удалить',
                        'imageUrl'=>null,
                        'options'=>array('class'=>'glyphicon glyphicon-remove'),
                    ),
                ),
            ),
        ),

    ));

    ?>
</div>

<div>
    <a href="<?=$this->createUrl('create')?>" class="btn btn-success">Добавить</a>
</div>