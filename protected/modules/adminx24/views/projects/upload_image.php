<!doctype html>
<html lang="ru">
<head>
    <title>Выберите файл</title>
    <link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/media/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/media/css/main.css"/>

    <script type="text/javascript" src="<?=Yii::app()->baseUrl?>/media/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->baseUrl?>/media/js/tinymce/tinymce.min.js"></script>
</head>
<body>
<div class="ui-container">
    <fieldset>
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'somemodel-form',
            'enableAjaxValidation' => false,
            //This is very important when uploading files
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
        ?>
        <div class="row">
            <?php
            $this->widget('ext.EAjaxUpload.EAjaxUpload', array(
                    'id'=>'EAjaxUpload',
                    'config' => array(
                        'action' => Yii::app( )->createUrl("/adminx24/projects/uploadimage"),
                        'template' => '<div class="qq-uploader"><div class="qq-upload-drop-area"><span>Drop files here to upload</span></div><div class="qq-upload-button">Загрузить...</div><ul class="qq-upload-list"></ul></div>',
                        'debug' => true,
                        'sizeLimit'=>2*1024*1024,// maximum file size in bytes
                        'minSizeLimit'=>1,// minimum file size in bytes
                        'allowedExtensions'=>array('jpg', 'svg'),
                        'htmlOptions' => array('id'=>'projects-form'),
                        'attribute' => 'file',
                        'autoUpload' => true,
                        'onComplete' => 'js:function(event, files, index, xhr, handler, callBack) {
                            if (typeof files !== "undefined"){
                                var url = admin_url + "/../uploads/images/content/" + files;
                                top.tinymce.activeEditor.execCommand("mceinsertContent", false, "<img src=\"" + url + "\">");
                            }else{
                                alert("Error upload file");
                            }
                            top.tinymce.activeEditor.windowManager.close();
                        }',
                        'messages'=>array(
                                          'typeError'=>"{file} Некорректное расширение. Только {extensions} доступны для загрузки.",
                                          'sizeError'=>"{file} слишком большой, максимальный размер файла составляет {sizeLimit}.",
                                          'minSizeError'=>"{file} не соответствует установленному минимум, который составляет {minSizeLimit}.",
                                          'emptyError'=>"{file} пустой. Выберите другой.",
                                          'onLeave'=>"Файлы загружены. Если вы хотите отменить загрузку, нажмите \"Cancel\"."
                                         ),
                        'showMessage'=>"js:function(message){ alert(message); }"
                    )
                )
            );
            ?>
        <?php $this->endWidget(); ?>
    </fieldset>
</div>
</body>
</html>
