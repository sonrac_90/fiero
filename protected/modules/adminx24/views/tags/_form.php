<?php

    /**
     * @var $this AdminController
     * @var $model Tags
     * @var $form CActiveForm
     */

    $form = $this->beginWidget('CActiveForm', array(
        'id'=>'article-form',
        'enableAjaxValidation'=>true,
        'enableClientValidation'=>true,
        'focus'=>array($model,'firstName'),
    ));

?>
<?php echo $form->errorSummary($model); ?>
<div class="tag_form">
    <div class="name">
        <div><?php echo $form->label($model,'name'); ?></div>
        <div><?php echo $form->textField($model,'name', array('class' => 'form-control')); ?></div>
        <div><?php echo $form->error($model,'name'); ?></div>
    </div>
    <div class="tag_id">
        <div><?php echo $form->label($model,'tag_id'); ?></div>
        <div><?php echo $form->textField($model,'tag_id', array('class' => 'form-control')); ?></div>
        <div><?php echo $form->error($model,'tag_id'); ?></div>
    </div>
    <div class="tag_id">
        <div><?php echo $form->label($model,'visible'); ?></div>
        <div><?php echo $form->checkBox($model,'visible'/*, array('data-toggle'=>"checkbox")*/); ?></div>
    </div>
</div>

<?php
    echo CHtml::submitButton(($model->isNewRecord) ? 'Добавить' : 'Сохранить', array('class' => 'btn btn-success'));
    $this->endWidget();
?>