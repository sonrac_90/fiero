<?php

    /**
     * @var $this AdminController
     * @var $model Tags
     */
    $this->title = 'Новый Тэг';

    $this->renderPartial('_form', array('model' => $model));