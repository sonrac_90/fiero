<?php

    /**
     * @var $this AdminController
     */

GHelper::registerJS(
    array(
        'tinymce/tinymce.min.js', //Подключаем текстовый редактор
        'admin/project_tinymce_init.js', //Подключаем текстовый редактор
        'admin/project_credits.js', //Подключаем текстовый редактор

        'bootstrap-tokenfield.js', //Подключаем плагин для формирования тэго
        'admin/project_tags.js', //Подключаем склипт для инициализации тэгов

        'plupload/moxie.min.js', //Подключаем текстовый редактор
        'plupload/plupload.min.js', //Подключаем текстовый редактор
        'admin/sliderSortable.js', //Подключаем текстовый редактор
    ),CClientScript::POS_END
);
$model = new Sliders();
$this->title = 'Слайдер';
$list = $model->createList();

?>

<div class="sliders">
    <?php foreach ($list as $value){
        ?>
        <div id="slide<?=$value['attr']['id']?>_<?=$value['attr']['position']?>"class="sliders_image">
            <a href="<?=$value['linkProject']?>" ><?=$value['nameProject']?></a>
            <img src="<?=$value['imageURL']?>">
            <a class="delete" href="#" >Удалить</a></div>
    <?php
    }?>
</div>