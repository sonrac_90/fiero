<?php
/* @var $this DefaultController */

GHelper::registerJS(array('admin/social.js'));
$this->breadcrumbs=array(
	$this->module->id
);
$this->title = 'Главная';
$imgLink = $imgLink = Yii::app()->baseUrl . About::PATH_TO_IMAGE . "/";
?>

<div class="contacts">
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'contacts-list',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'itemsCssClass' => 'table table-striped',
        'enableHistory' => true,
        'columns' => array(
            array(
                'name' => 'phone',
                'filter' => false,
            ),
            array(
                'name' => 'email',
                'filter' => false
            )
        )
    ));

    $model = new ContactSocial();
    $allSocial = $model->findAll();
?>

    <div class="social">
        <?php foreach ($allSocial as $allSoc) {?>
            <div class="rowSocial" id="num<?=$allSoc->id?>_<?=$allSoc->position?>">
                <span><?=$allSoc->name?></span>
                <span><img src="<?=$imgLink?>/<?=$allSoc->img?>" width="20" height="20"/></span>
                <span><a href="<?=$allSoc->link?>" ><?=$allSoc->link?></a></span>
                <span class="button-column">
                    <a class="glyphicon glyphicon-pencil" title="Изменить" href="<?=$this->createUrl("update")?>/id/<?=$allSoc->id?>">Изменить</a>
                    <a class="glyphicon glyphicon-remove" title="Удалить" href="<?=$this->createUrl("delete")?>/id/<?=$allSoc->id?>">Удалить</a>
                </span>
            </div>
        <?php }?>
    </div>
    <?php echo CHtml::button('Добавить', array('id' => 'addSocial', 'class' => 'btn btn-success'))?>
    <div id="addNew" ></div>



</div>
