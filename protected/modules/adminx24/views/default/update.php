<?php
/* @var $this DefaultController */
GHelper::registerJS(array('admin/socialImage.js'));
$this->breadcrumbs=array(
	$this->module->id
);

$form = $this->beginWidget('CActiveForm', array(
        'id' => 'socialForm'
    ));
$ext = GFileUploader::pathExtension($model->img);
$fid = GFileUploader::pathFilename($model->img);
$fname =  $fid . '.' . $ext;


$maxAnsTime = Yii::app()->db
    ->createCommand("SELECT max(position) as max FROM " . $model->tableName())
    ->where(' 1 ')
    ->queryRow();

$imgLink = Yii::app()->baseUrl . About::PATH_TO_IMAGE . "/" . $fname;
$model->position = $maxAnsTime['max'] + 1;
echo $form->errorSummary($model);
?>
<div class="social">
    <div class="social">
        <div class="name">
            <div><?php echo $form->label($model, 'name');?></div>
            <div><?php echo $form->textField($model, 'name', array('class' => 'form-control'));?></div>
            <div><?php echo $form->error($model, 'name');?></div>
        </div>
        <div class="image">
            <div><?php echo $form->label($model, 'img');?></div>
            <div><?php echo $form->hiddenField($model, 'img', array('class' => 'form-control', 'id' => 'socialImageLink'));?></div>
            <div><?php echo CHtml::image($imgLink, 'Изображение', array('width' => '50', 'height' => '50'))?></div>
            <div id="" style="text-align: center; overflow: hidden; width: 150px; height: 50px; border: 1px solid #038f1a; background-color: #038f1a;">
                <div style="color: #fff; font-size: 20px; font-weight: bold;">Выбрать файл</div>
                <?php echo CHtml::fileField('file', '', array('id' => 'fileAbout', 'name' => 'uploadImage', 'style' => 'margin-top: -50px; margin-left:-410px;
                -moz-opacity: 0; filter: alpha(opacity=0); opacity: 0; font-size: 150px; height: 100px;'))?>
            </div>
        </div>
        <div class="link">
            <div><?php echo $form->label($model, 'link');?></div>
            <div><?php echo $form->textField($model, 'link', array('class' => 'form-control'));?></div>
            <div><?php echo $form->error($model, 'link');?></div>
                <div><?php echo $form->hiddenField($model, 'position', array('class' => 'form-control'));?></div>
            <div><?php echo $form->error($model, 'position');?></div>
        </div>
        <?php
        echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-success'));

        $this->endWidget();
    ?>
</div>