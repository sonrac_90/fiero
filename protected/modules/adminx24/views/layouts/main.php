<?php

    /**
     * @var $this AdminController
     */

?>
<!doctype html>
<html lang="<?= Yii::app()->language ?>">
<head>
    <meta charset="UTF-8">
    <?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
    <script type="text/javascript" src="<?=Yii::app()->baseUrl?>/media/js/jquery-1.10.2.js"></script>
    <script type="text/javascript">
        var base_url = "<?= Yii::app()->baseUrl?>";
        var admin_url = "<?= $this->createUrl('default/index')?>";
        var token = "<?= Yii::app()->request->csrfToken?>";
    </script>
    <link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/media/css/bootstrap.min.css"/>

    <!--<link rel="stylesheet" href="<?/*=Yii::app()->baseUrl*/?>/media/css/bootstrap-theme.min.css"/>-->
    <!--<link rel="stylesheet" href="<?/*=Yii::app()->baseUrl*/?>/media/css/flat-ui.css"/>-->
    <title><?=$this->pageTitle?></title>
    <?php
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        GHelper::registerCSS(
            array(
                /*'bootstrap.min.css',
                'bootstrap-theme.min.css',*/
                //'theme.css',
                'flat-ui.css',
                'main_admin.css',
            )
        );
        GHelper::registerJS(
            array(
                'flat-ui/jquery.ui.touch-punch.min.js',

                'bootstrap.min.js',

                'flat-ui/bootstrap-select.js',
                'flat-ui/bootstrap-switch.js',
                'flat-ui/flatui-checkbox.js',
                'flat-ui/flatui-radio.js',
                'flat-ui/jquery.tagsinput.js',
                'flat-ui/jquery.placeholder.js',
                'flat-ui/jquery.stacktable.js',

                'main_admin.js',
            )
        );
    ?>
</head>
<body>
    <div id="wrap">
        <div class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!--<a href="#" class="navbar-brand"><?/*=Yii::app()->name*/?></a>-->
                </div>
                <div class="collapse navbar-collapse">
                    <?php
                        $this->widget('zii.widgets.CMenu',
                            array(
                                'htmlOptions' => array(
                                    'class' => 'nav navbar-nav',
                                ),
                                'items'=>array(
                                    array(
                                        'label'=>'На главную',
                                        'url'=>$this->createUrl('default/index'),
                                        'active' => (Yii::app()->controller->id == 'default' && Yii::app()->controller->action->id == 'index'),
                                    ),
                                    array(
                                        'label'=>'Тэги',
                                        'url'=>$this->createUrl('tags/index'),
                                        'active' => (Yii::app()->controller->id == 'tags'),
                                    ),
                                    array(
                                        'label'=>'Проекты',
                                        'url'=>$this->createUrl('projects/index'),
                                        'active' => (Yii::app()->controller->id == 'projects'),
                                    ),
                                    array(
                                        'label'=>'Слайдер',
                                        'url'=>$this->createUrl('sliders/index'),
                                        'active' => (Yii::app()->controller->id == 'sliders'),
                                    ),
                                    array(
                                        'label'=>'Контакты',
                                        'url'=>$this->createUrl('contacts/index'),
                                        'active' => (Yii::app()->controller->id == 'contacts'),
                                    ),
                                    array(
                                        'label'=>'О нас',
                                        'url'=>$this->createUrl('about/index'),
                                        'active' => (Yii::app()->controller->id == 'about'),
                                    ),
                                )
                            )
                        );
                    ?>
                    <div class="navbar-right navbar-form">
                        <a href="<?= $this->createUrl('default/logout') ?>" class="btn btn-default">Выход(<?=Yii::app()->user->name?>)</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="page-header">
                <h1><?=$this->title?></h1>
            </div>
            <div class="content">
                <?=$content?>
            </div>
        </div>
    </div>
    <div id="footer">
        <div class="container">
            <p class="text-muted">© <?=Yii::app()->name?></p>
        </div>
    </div>
</body>
</html>