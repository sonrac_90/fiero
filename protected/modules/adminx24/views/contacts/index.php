<div class="contacts">
<?php
$this->title = 'Контакты';
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'contacts-list',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'itemsCssClass' => 'table table-striped',
    'enableHistory' => true,
    'columns' => array(
        array(
            'name' => 'phone',
            'filter' => false,
        ),
        array(
            'name' => 'email',
            'filter' => false
        ),
        array( //Кнопочки
            'class'=>'CButtonColumn',
            'template'=>'{update}',
            'buttons'=>array(
                'update' => array(
                    'label'=>'Изменить',
                    'imageUrl'=>null,
                    'options'=>array('class'=>'glyphicon glyphicon-pencil'),
                    'visible' => 'Yii::app()->user->checkAccess("admin")',
                ),
            )
        )
    )
));
