<?php
/* @var $this Contacts */
GHelper::registerJS(array('admin/contactsImage.js'));
$this->breadcrumbs=array(
	$this->module->id
);

$form = $this->beginWidget('CActiveForm', array(
        'id' => 'contactsForm'

    ));
$imgLink = $imgLink = Yii::app()->baseUrl . About::PATH_TO_IMAGE . "/" . $model->img;
?>
<div class="contact_form">
    <div class="phone">
        <div><?php echo $form->label($model, 'phone');?></div>
        <div><?php echo $form->textField($model, 'phone', array('class' => 'form-control'));?></div>
        <div><?php echo $form->error($model, 'phone');?></div>
    </div>
    <div class="email">
        <div><?php echo $form->label($model, 'email');?></div>
        <div><?php echo $form->textField($model, 'email', array('class' => 'form-control'));?></div>
        <div><?php echo $form->error($model, 'email');?></div>
    </div>
    <div class="image">
        <div><?php echo $form->label($model, 'img');?></div>
        <div><?php echo $form->hiddenField($model, 'img', array('class' => 'form-control', 'id' => 'contactsImageLink'));?></div>
        <div><?php echo CHtml::image($imgLink, 'Изображение', array('width' => '50', 'height' => '50'))?></div>
        <div id="" style="text-align: center; overflow: hidden; width: 150px; height: 50px; border: 1px solid #038f1a; background-color: #038f1a;">
            <div style="color: #fff; font-size: 20px; font-weight: bold;">Выбрать файл</div>
            <?php echo CHtml::fileField('file', '', array('id' => 'fileAbout', 'name' => 'uploadImage', 'style' => 'margin-top: -50px; margin-left:-410px;
                -moz-opacity: 0; filter: alpha(opacity=0); opacity: 0; font-size: 150px; height: 100px;'))?>
        </div>
    </div>

    <?php
        echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-success'));
        $this->endWidget();
    ?>
</div>