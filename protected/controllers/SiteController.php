<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/***********************************************************************************************/

    public function actionIndex()
	{
        $criteria = new CDbCriteria();
        $criteria->order = ' position ASC ';

        $tags = Tags::model()->findAll($criteria);
        $projects = Projects::model()->findAll();

		$this->render('index', array(
            'projects' => $projects,
            'tags' => $tags
        ));
	}

    /*public function actionAbout()
    {
        $this->render('about');
    }

    public function actionContacts()
    {
        $this->render('contacts');
    }

    public function actionProjects($tag = 0, $page = 0)
    {
        if (intval($page) > 0) {
            $page--;
        }

        $criteria = new CDbCriteria();
        if (intval($tag) > 0) {
            $criteria->distinct = true;
            $criteria->join = ' INNER JOIN dvlp_project_tags on t.id = dvlp_project_tags.project_id ';
            $criteria->condition = ' dvlp_project_tags.tag_id = '.intval($tag).' ';
        }
        $criteria->order = ' position DESC ';
        $projects_count = Projects::model()->count($criteria);

        $pages = new CPagination($projects_count);
        $pages->pageSize = 12;
        $pages->currentPage = $page;
        $pages->applyLimit($criteria);

        $projects = Projects::model()->findAll($criteria);

        $this->render('projects', array(
            'tags' => Tags::model()->findAll(),
            'selected_tag' => intval($tag),
            'pages' => $pages,
            'projects' => $projects
        ));
    }

    public function actionProject($id = 0)
    {
        $this->render('project', array('project' => Projects::model()->findByPk(intval($id))));
    }*/

    /*********************************************************************************************/

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the login page
	 */
	/*public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}*/

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	/*public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}*/
}