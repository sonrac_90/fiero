<?php

class ApiController extends Controller
{

    public function actionGet_index()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = ' visible = 1 ';
        $criteria->order = ' position ASC ';
        $criteria->limit = 5;

        $this->renderPartial('/site/__index', array(
            'projects' => Projects::model()->findAll($criteria)
        ));
    }

    public function actionGet_projects()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = ' visible = 1 ';
        $criteria->order = ' position ASC ';

        $this->renderPartial('/site/__projects', array(
            'projects' => Projects::model()->findAll($criteria)
        ));
    }

    public function actionGet_work($id = 0)
    {
        if (intval($id) > 0) {
            $this->renderPartial('/site/__work', array(
                'work' => Projects::model()->findByPk(intval($id))
            ));
        }
    }

    public function actionGet_about()
    {
        $this->renderPartial('/site/__about', array());
    }

    public function actionGet_downloads()
    {
        $this->renderPartial('/site/__downloads', array());
    }

    public function actionGet_contacts()
    {
        $this->renderPartial('/site/__contacts', array());
    }

}