<?php

class WorkController extends Controller
{
    public function actions()
    {
        return array(
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    public function actionIndex($id = 0)
    {
        if (intval($id) > 0) {
            $work = Projects::model()->findByPk(intval($id));

            if (isset($work) && !is_null($work)) {
                $this->render('index', array(
                    'work' => $work
                ));
            } else {
                $this->redirect(Yii::app()->createUrl('projects'));
            }
        } else {
            $this->redirect(Yii::app()->createUrl('projects'));
        }
    }

    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('/site/error', $error);
        }
    }
}