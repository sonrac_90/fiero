<?php

return array(
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name'=>'Fiero Animals',

	'preload'=>array('log'),

	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.helpers.*',
		'application.helpers.GFile.*',
	),

	'modules'=>array(
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'p',
			'ipFilters'=>array('127.0.0.1', '::1', '85.238.102.153'),
		),

        'adminx24' => array(),

	),

	'components'=>array(

		'user'=>array(
			'allowAutoLogin'=>true,
		),

        'authManager' => array(
            'class' => 'PhpAuthManager',
            'defaultRoles' => array('guest'),
        ),

		'urlManager'=>array(
			'urlFormat'=>'path',
            'showScriptName'=>false,
            'caseSensitive'=>false,
			'rules'=>array(

                //'<action:\w+>' => 'site/<action>',
                ''              => 'site/index',
                'site'          => 'site/index',
                'main'          => 'site/index',
                'index'         => 'site/index',
				'about'         => 'site/index',
				'contacts'      => 'site/index',
				'downloads'     => 'site/index',
				'projects'      => 'site/index',
				'work<id:\d+>'  => 'site/index',

                /*'site'          => 'site/index',
                'projects'      => 'projects/index',
                'work'          => 'work/index',
                'downloads'     => 'downloads/index',
                'contacts'      => 'contacts/index',
                'about'         => 'about/index',*/


                /*Правила для админки*/
                '<module:\w+>' => '<module>/default/index',
                '<module:\w+>/login' => '<module>/<controller>/login',
                '<module:\w+>/<controller:\w+>' => '<module>/<controller>/index',
                /*Правила для админки*/

				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
        
        'db'=>array(
            'connectionString'   => 'mysql:host=' . SERVER . ';dbname=' . DB_NAME,
            'emulatePrepare'     => true,
            'username'           => DB_USERNAME,
            'password'           => DB_PASSWORD,
            'charset'            => 'utf8',
            'tablePrefix'        => DB_PREFIX,
            'enableProfiling'    => true,
            'enableParamLogging' => true

        ),
		'errorHandler'=>array(
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
	),

    'sourceLanguage' => 'ru', //Основной язык
    'language'       => 'ru', //Текущий язык

	'params'=>array(
		'adminEmail' => 'webmaster@example.com',
	),
);