<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>Fiero Animals</title>

    <script type="text/javascript">
        var baseUrl = '<?=Yii::app()->getBaseUrl(true)?>';
        var CURRENT_PAGE = '<?=preg_replace("/[^a-z0-9]/", "", str_replace(Yii::app()->getBaseUrl(true).'/', '', 'http://'.$_SERVER['SERVER_NAME'].Yii::app()->request->requestUri))?>';
    </script>

    <link rel="stylesheet" href="<?=Yii::app()->getBaseUrl(true)?>/css/main.css" type="text/css" />

    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/js/libs/cookie.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/js/dvlp.js"></script>

    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/js/libs/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/js/libs/jquery.history.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/js/libs/jquery.resizer.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/js/libs/jquery.megaslider.js"></script>

    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/js/history.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/js/main_menu.js?asd=<?=rand(0, 1000)?>"></script>
    <script type="text/javascript" src="<?=Yii::app()->getBaseUrl(true)?>/js/main.js?asd=<?=rand(0, 1000)?>"></script>
</head>

<body>
    <?php echo $content; ?>
</body>

</html>
