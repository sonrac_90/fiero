<?php
$model = new Contacts();
$attr = $model->findByPk(1);
foreach($attr as $key=>$val) $model->$key = $val;
?>
<div class="asd-wrap">

    <div class="b-header b-header_top">
        <div class="b-wrap_in">
            <a href="#" class="b-header__img"><img src="<?=Yii::app()->getBaseUrl(true)?>/img/header-main.png" /></a>
            <div class="b-header__menu">
                <a href="<?=Yii::app()->createUrl('/')?>" class="b-header__menu_item">Home</a>
                <a href="<?=Yii::app()->createUrl('site/projects')?>" class="b-header__menu_item">Projects</a>
                <a href="<?=Yii::app()->createUrl('site/about')?>" class="b-header__menu_item">About</a>
                <a href="<?=Yii::app()->createUrl('site/contacts')?>" class="b-header__menu_item b-header__menu_item_active">Contacts</a>
            </div>
            <div class="asd-clear"></div>
        </div>
    </div>

    <div class="b-resize_content">
        <div class="asd-wrap">
            <img class="bg" src="<?=Yii::app()->getBaseUrl(true)?><?=About::PATH_TO_IMAGE?>/<?=$model->img?>" />
        </div>

        <div class="b-head__bottom">
            <div class="asd-wrap b-overlay"></div>
            <div class="b-wrap_in2 b-head__bottom_content">
                <h1>Contacts</h1>
                <div class="b-contacts_wrap">
                    <div class="b-contacts_text"><?=$model->email?></div>
                    <div class="b-contacts_text"><?=$model->phone?></div>
                    <div class="asd-clear"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="b-footer b-footer_bottom">
        <div class="b-wrap_in2">
            <a class="b-footer__email" href="mailto:<?=$model->email?>">box@fieroanimals.com</a>
            <div class="b-footer__tel"><?=$model->phone?></div>

            <div class="b-footer__socials">
            <?php
                $social = new ContactSocial();
                $allSocial = $social->findAll();
                foreach($allSocial as $newSocial){
                    ?>
                    <a class="" href="<?=$newSocial['link']?>"><img src="<?=Yii::app()->getBaseUrl(true)?><?=About::PATH_TO_IMAGE?>/<?=$newSocial['img']?>" width="20" height="20"></a>
            <?php
                }
            ?>
            </div>

            <div class="b-footer__download">
                    <span>
                        Downloads
                        <div class="b-download_panel_wrap" style="display: none;">
                            <em></em>
                            <div class="b-download_panel">
                                <div class="b-download_item"><a href="#">People Showcase</a>(17 Mb)</div>
                                <div class="b-download_item"><a href="#">Food & Drinks Showcase</a>(11 Mb)</div>
                                <div class="b-download_item"><a href="#">Transport Showcase</a>(20 Mb)</div>
                                <div class="b-download_item"><a href="#">Landscape Showcase</a>(24 Mb)</div>
                            </div>
                        </div>
                    </span>
            </div>

            <div class="asd-clear"></div>
        </div>
    </div>

</div>