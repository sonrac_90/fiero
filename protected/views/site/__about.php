<div class="b-main_bg b-red_bg">
    <div class="b-main_bg_title">About us</div>
    <div class="b-main_bg_line"></div>
    <div class="b-main_bg_text">Other travelling salesmen live a life of luxury instance whenever </div>
</div>

<div class="b-about_block b-aspect_ratio" data-aspect-ratio="30">
    <div class="b-block b-block_50">
        <img class="bg bg-top" src="<?=Yii::app()->getBaseUrl(true)?>/img/about_1.jpg" />
    </div>
    <div class="b-block b-block_50">
        <div class="b-about_text_wrap">
            <div class="b-title">Fiero Animals is a creative retouching and CGi studio </div>
            <div class="b-text">
                <span class="b-line_in_text"></span> specializing in production of high quality commercial CG and photo based imagery for the
                advertising and entertainment industries. We also provide a wide variety of services such as concept art, creative direction,
                art direction, photo shooting and everything else you need in order to build a visual campaign from scratch to a final visualization.
            </div>
        </div>
    </div>
    <div class="asd-clear"></div>
</div>

<div class="b-about_block b-aspect_ratio" data-aspect-ratio="30">
    <div class="b-block b-block_50">
        <div class="b-about_text_wrap">
            <div class="b-title">
                For the past 5 years of experience we have built up an impressive client list that features many top advertising agencies and the worlds' best known brands
            </div>
            <div class="b-text">
                <span class="b-line_in_text"></span> We established a reputation of studio that delivers exceptional images on schedule and on budget.
                Attention to detail and a creative approach to every project as long as a complex combination of high-end creative retouching and image
                manipulation with 3D digital imaging techniques are fundamentals of our workflow.
            </div>
        </div>
    </div>
    <div class="b-block b-block_50">
        <div class="b-block b-block_50">
            <img class="bg bg-top" src="<?=Yii::app()->getBaseUrl(true)?>/img/about_2.jpg" />
        </div>
        <div class="b-block b-block_50">
            <div class="b-half_block">
                <img class="bg bg-top" src="<?=Yii::app()->getBaseUrl(true)?>/img/about_3.jpg" />
            </div>
            <div class="b-half_block b-about_twitter_bg">
                <div class="b-about_twitter_in">
                    <div class="b-about_twitter_img"></div>
                    <div class="b-about_twitter_text">Some hot new pictures for Mercedes GL AMG promo campaign</div>
                    <a class="b-about_twitter_link" href="#">http://buff.ly/1m9uoEV</a>
                    <a class="b-about_twitter_follow b-slide_link" href="#" target="_blank">Follow us</a>
                </div>
            </div>
        </div>
        <div class="asd-clear"></div>
    </div>
    <div class="asd-clear"></div>
</div>

<div class="b-about_clients_wrap">
    <div class="b-about_clients_title">Our clients</div>

    <div class="b-about_clients">
        <div class="b-about_clients_block">
            <div class="b-about_clients_items">
                <div class="b-about_clients_item_title"><span class="b-line_in_text"></span> Agencies</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
            </div>
            <div class="b-about_clients_items">
                <div class="b-about_clients_item_title"><span class="b-line_in_text"></span> Automotive</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
            </div>
        </div>
        <div class="b-about_clients_block">
            <div class="b-about_clients_items">
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
            </div>
            <div class="b-about_clients_items">
                <div class="b-about_clients_item_title"><span class="b-line_in_text"></span> FMCG</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
            </div>
        </div>
        <div class="b-about_clients_block">
            <div class="b-about_clients_items">
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
            </div>
            <div class="b-about_clients_items">
                <div class="b-about_clients_item_title"><span class="b-line_in_text"></span> Technology</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
            </div>
        </div>
        <div class="b-about_clients_block">
            <div class="b-about_clients_items">
                <div class="b-about_clients_item_title"><span class="b-line_in_text"></span> Technology</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
                <div class="b-about_clients_item_text">Mitsubishi</div>
            </div>
        </div>
        <div class="asd-clear"></div>
    </div>
</div>