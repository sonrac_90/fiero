<?php
    /**
    * @var $projects Projects
    */

    function get_project_str($project)
    {
        $double = (intval($project['new_size']) === 2) ? true : false;

        $proj_img = '';
        if (isset($project['item']->images)) {
            foreach ($project['item']->images as $project_img) {
                $proj_img = Yii::app()->getBaseUrl(true).Projects::IMAGE_PATH.GHelper::dynamicPath($project_img->id).'/'.$project_img->id.'.'.$project_img->ext;
                break;
            }
        }

        $transform_percents = '';
        $place = (($project['place'] - 1) % 4) + 1;
        switch ($place) {
            case 1: $transform_percents = ($double) ? 200 : 400; break;
            case 2: $transform_percents = ($double) ? 150 : 300; break;
            case 3: $transform_percents = ($double) ? 100 : 200; break;
            case 4: $transform_percents = ($double) ? 50 : 100; break;
        }
        $transform_percents = 400;
        $transform = ' style="-webkit-transform:translate3d('.$transform_percents.'%, 0, 0);" ';

        return '<div class="b-block '.(($double) ? 'b-block_50' : 'b-block_25').'" '.$transform.'>
                    <div class="b-project '.(($double) ? 'b-project_double' : '').' asd-wrap">
                        <div class="b-project_bg asd-wrap"><img class="bg" src="'.$proj_img.'" /></div>
                        <div class="b-project_content asd-wrap">
                            <div class="b-project_content_in">
                                <div class="b-project_sub_descr">Top flight magazine</div>
                                <div class="b-project_title">'.$project['item']->name.'</div>
                                <div class="b-project_text"><span class="b-line_in_text"></span> '.$project['item']->short_name.'</div>
                            </div>
                            <a class="b-project_link" href="javascript:set_page(\'work'.$project['item']->id.'\')">Take a look</a>
                        </div>
                    </div>
                </div>';
    }

    function get_nearest_place($json_data, $size)
    {
        $current_place = 1;

        if (count($json_data) > 0) {
            foreach ($json_data as $j => $project) {
                if ($current_place == $project['place']) {
                    $current_place = $project['place'] + $project['new_size'];
                } else {
                    if ($current_place % 4 == 0 && $size == 2) {
                        if ($current_place < $project['place']) {
                            $current_place = $project['place'] + $project['new_size'];
                        } else {
                            $current_place++;
                        }
                    } else {
                        return $current_place;
                    }
                }
            }

            if ($current_place % 4 == 0 && $size == 2) {
                return ($current_place + 1);
            }
        }

        return $current_place;
    }

    /************************************************************************************************************************************************/

    // меняем расположение программно по принципу 1, 6, 7, 12, 13, 18, 19, 24 и т.д.
    foreach ($projects as $i => $project)
    {
        if (($i+1) % 6 === 1 || ($i+1) % 6 === 0) {
            $project->preview_size = 2;
        } else {
            $project->preview_size = 1;
        }
    }

    $json_data_new = array();
    foreach ($projects as $i => $project)
    {
        // находим ближайшее свободное место для нашего блока
        $nearest_place = get_nearest_place($json_data_new, intval($project->preview_size));
        $nearest_place_for_2 = get_nearest_place($json_data_new, 2);
        $nearest_place_for_1 = get_nearest_place($json_data_new, 1);

        // обычная ситуация когда блок влазит в ближайшее место
        if (intval($project->preview_size) === 1 || (intval($project->preview_size) === 2 && $nearest_place_for_2 === $nearest_place_for_1)) {
            $json_data_new[] = array('item' => $project, 'place' => $nearest_place, 'new_size' => intval($project->preview_size));
        }
        // если блок двойной и не влазит
        else if (intval($project->preview_size) === 2 && $nearest_place_for_2 !== $nearest_place_for_1) {
            // если он последний, то чтобы не переносить его на новую строку делаем его одинарным
            if (($i+1) === count($projects)) {
                $json_data_new[] = array('item' => $project, 'place' => $nearest_place_for_1, 'new_size' => 1);
            }
            // или переносим на новую строку, если за ним еще блоки будут
            else {
                $json_data_new[] = array('item' => $project, 'place' => $nearest_place, 'new_size' => intval($project->preview_size));
            }
        }

        // сортируем по place ASC
        usort($json_data_new, function($a, $b) {
            return ($a['place'] - $b['place']);
        });
    }

    $projects_str = '';
    $i = 0;
    while ($i < count($json_data_new)) {
        $projects_str .= '<div class="b-projects_block b-aspect_ratio" data-aspect-ratio="25">';
        
        $new_project_place = 0;
        $old_project_place = 0;

        for ($k = 1; $k <= 4; $k++) {
            if ($old_project_place > $new_project_place) {
                break;
            } else {
                $projects_str .= get_project_str($json_data_new[$i]);

                $old_project_place = $new_project_place;
                $i++;
                if (isset($json_data_new[$i])) {
                    $new_project_place = ($json_data_new[$i]['place'] - 1) % 4;
                } else {
                    break;
                }
            }
        }

        $projects_str .= '<div class="asd-clear"></div></div>';
    }

    echo $projects_str;


























