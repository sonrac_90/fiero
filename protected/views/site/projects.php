<div class="b-wrapper">

    <div class="b-header">
        <div class="b-wrap_in">
            <a href="#" class="b-header__img"><img src="<?=Yii::app()->getBaseUrl(true)?>/img/header-main.png" /></a>
            <div class="b-header__menu">
                <a href="<?=Yii::app()->createUrl('/')?>" class="b-header__menu_item">Home</a>
                <a href="<?=Yii::app()->createUrl('site/projects')?>" class="b-header__menu_item b-header__menu_item_active">Projects</a>
                <a href="<?=Yii::app()->createUrl('site/about')?>" class="b-header__menu_item">About</a>
                <a href="<?=Yii::app()->createUrl('site/contacts')?>" class="b-header__menu_item">Contacts</a>
            </div>
            <div class="asd-clear"></div>
        </div>
    </div>

    <div class="b-projects_content">
        <div class="b-wrap_in">
            <div class="b-projects_head">
                <div class="b-projects_head_title">Projects</div>

                <div class="b-pages_head_item <?php if ($selected_tag == 0) { echo 'b-pages_item_selected'; } ?>">
                    <a class="b-pages_item_text" href="<?=Yii::app()->createUrl('site/projects')?>">Show all</a>
                </div>
                <?php
                    if (isset($tags)) {
                        foreach ($tags as $tag) {
                ?>
                            <div class="b-pages_head_item <?php if ($selected_tag == $tag->id) { echo 'b-pages_item_selected'; } ?>">
                                <a class="b-pages_item_text" href="<?=Yii::app()->createUrl('site/projects/tag/'.$tag->id)?>"><?=$tag->name?></a>
                            </div>
                <?php
                        }
                    }
                ?>
                <div class="asd-clear"></div>
            </div>
        </div>

        <div class="b-projects_in_wrap">
            <div class="b-projects_in">

                <?php if (!($pages->currentPage > 0)) { ?>
                    <div class="b-project b-project_quad">
                        <div class="asd-wrap">
                            <div class="b-project_video_wrap">
                                <iframe src="//player.vimeo.com/video/40247627" style="width: 100%; height: 100%;" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            </div>
                            <div class="b-project_video_text_wrap">
                                <div class="b-project_video_text">Making of <b>Pedigree</b></div>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <?php foreach ($projects as $project) { ?>
                    <div class="b-project">
                        <div class="asd-wrap">
                            <img class="asd-wrap" src="<?=Yii::app()->getBaseUrl(true)?><?=Projects::IMAGE_SMALL_PATH?>projects/<?=GHelper::dynamicPath($project->id)?>/<?=$project->id?>.png" />
                        </div>
                        <div class="b-project_panel">
                            <div class="asd-wrap b-project_panel_overlay"></div>
                            <div class="b-project_panel_in">
                                <a class="b-project_panel_text" href="<?=Yii::app()->createUrl('site/project/id/'.$project->id)?>"><?=$project->name?></a>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <div class="asd-clear"></div>
            </div>
        </div>

        <div class="b-wrap_in2">
            <div class="b-projects_pages">
                <?php
                    $this->widget('CLinkPager',
                        array(
                            'htmlOptions' => array(
                                'class' => 'yii-pager'
                            ),
                            'internalPageCssClass' => 'b-pages_item',
                            'selectedPageCssClass' => 'b-pages_item_selected',

                            'previousPageCssClass' => 'b-pages_item',
                            'nextPageCssClass' => 'b-pages_item',

                            'prevPageLabel' => 'Previous',
                            'nextPageLabel' => 'Next',

                            'header' => '',
                            'pages' => $pages,
                        )
                    );
                ?>
                <div class="asd-clear"></div>
            </div>
        </div>
    </div>

    <div class="b-footer b-footer_simple">
        <div class="b-wrap_in2">
            <?php
            $model = new Contacts();
            $attr = $model->findByPk(1);
            foreach($attr as $key=>$val) $model->$key = $val;
            ?>
            <a class="b-footer__email" href="mailto:<?=$model->email?>">box@fieroanimals.com</a>
            <div class="b-footer__tel"><?=$model->phone?></div>

            <div class="b-footer__socials">
                <?php
                $social = new ContactSocial();
                $allSocial = $social->findAll();
                foreach($allSocial as $newSocial){
                    ?>
                    <a class="" href="<?=$newSocial['link']?>"><img src="<?=Yii::app()->getBaseUrl(true)?><?=About::PATH_TO_IMAGE?>/<?=$newSocial['img']?>" width="20" height="20"></a>
                <?php
                }
                ?>
            </div>

            <div class="b-footer__download">
                    <span>
                        Downloads
                        <div class="b-download_panel_wrap" style="display: none;">
                            <em></em>
                            <div class="b-download_panel">
                                <div class="b-download_item"><a href="#">People Showcase</a>(17 Mb)</div>
                                <div class="b-download_item"><a href="#">Food & Drinks Showcase</a>(11 Mb)</div>
                                <div class="b-download_item"><a href="#">Transport Showcase</a>(20 Mb)</div>
                                <div class="b-download_item"><a href="#">Landscape Showcase</a>(24 Mb)</div>
                            </div>
                        </div>
                    </span>
            </div>

            <div class="asd-clear"></div>
        </div>
    </div>

</div>