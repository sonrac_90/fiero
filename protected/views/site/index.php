<div class="b-content" r-width="1400">
    <div class="b-content_in font-autosize" f-size="15">

        <!---------------------------------------------------------- PRELOADER ----------------------------------------------------------->
        <div class="b-preloader b-content_100 asd-wrap">
            <div class="b-preloader_in asd-pos-center">
                <div class="b-preloader_item b-preloader_item_1"></div>
                <div class="b-preloader_item b-preloader_item_2"></div>
                <div class="b-preloader_item b-preloader_item_3"></div>
                <div class="b-preloader_item b-preloader_item_4"></div>
                <div class="b-preloader_item b-preloader_item_5"></div>
            </div>
        </div>
        <!--------------------------------------------------------- #PRELOADER ----------------------------------------------------------->

        <!---------------------------------------------------------- HEADER ----------------------------------------------------------->
        <div class="b-header">
            <a class="b-header_title" href="javascript:set_page('index')">
                <img class="asd-img-width" src="<?=Yii::app()->getBaseUrl(true)?>/img/header-main.png" />
            </a>
            <div class="b-header_menu">
                <a class="b-header_menu_item b-menu_item" href="javascript:set_page('projects')"><div class="b-menu_item_bg asd-wrap"></div><span>Projects</span></a>
                <a class="b-header_menu_item b-menu_item" href="javascript:set_page('about')"><div class="b-menu_item_bg asd-wrap"></div><span>About</span></a>
                <a class="b-header_menu_item b-menu_item" href="javascript:set_page('downloads')"><div class="b-menu_item_bg asd-wrap"></div><span>Downloads</span></a>
                <a class="b-header_menu_item b-menu_item" href="javascript:set_page('contacts')"><div class="b-menu_item_bg asd-wrap"></div><span>Contacts</span></a>

                <a class="b-header_menu_item b-social b-social_tw" href="#" target="_blank">
                    <div class="b-social_in b-social_in_top asd-wrap"></div><div class="b-social_in b-social_in_bottom asd-wrap"></div>
                </a>
                <a class="b-header_menu_item b-social b-social_fb" href="#" target="_blank">
                    <div class="b-social_in b-social_in_top asd-wrap"></div><div class="b-social_in b-social_in_bottom asd-wrap"></div>
                </a>
                <a class="b-header_menu_item b-social b-social_be" href="#" target="_blank">
                    <div class="b-social_in b-social_in_top asd-wrap"></div><div class="b-social_in b-social_in_bottom asd-wrap"></div>
                </a>
            </div>
            <div class="asd-clear"></div>
        </div>

        <div class="b-projects_menu" style="display: none;">
            <div class="b-projects_menu_in" style="height: 0;">
                <div class="b-projects_menu_items" style="display: none;">
                    <div class="b-projects_menu_left">
                        <a class="b-projects_menu_item_showreel" href="#">Showreel</a>
                    </div>
                    <div class="b-projects_menu_right">
                        <a class="b-projects_menu_item b-projects_menu_item_-1 b-projects_menu_item_selected" data-item-id="-1" href="javascript:projects_menu_update(-1)">Show All</a>
                        <?php
                            if (isset($tags)) {
                                foreach ($tags as $indx => $tag) {
                        ?>
                                    <a class="b-projects_menu_item b-projects_menu_item_<?=$tag->id?>" data-item-id="<?=$tag->id?>" href="javascript:projects_menu_update(<?=$tag->id?>)"><?=$tag->name?><em></em></a>
                        <?php
                                }
                            }
                        ?>
                    </div>
                </div>

                <div class="b-work_menu_items" style="display: none;">
                    <div class="b-projects_menu_left">
                        <div class="b-work_menu_title">Projects - Zatecky Gus / BBDO Moscow</div>
                    </div>
                    <div class="b-projects_menu_right">
                        <div class="b-work_menu_buttons">
                            <a class="b-work_menu_button b-work_menu_button_share" href="#"></a>
                            <a class="b-work_menu_button b-work_menu_button_left" href="#"></a>
                            <a class="b-work_menu_button b-work_menu_button_right" href="#"></a>
                            <a class="b-work_menu_button b-work_menu_button_close" href="javascript:set_page('projects')"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!---------------------------------------------------------- #HEADER ---------------------------------------------------------->

        <!---------------------------------------------------------- CONTENT ---------------------------------------------------------->
        <div class="b-pages">
            <script type="text/javascript">
                var pages = [
                    { 'title' : 'Main', 'name' : 'index', 'menu_button' : '' },
                    { 'title' : 'Projects', 'name' : 'projects', 'menu_button' : '0' },

                    <?php foreach ($projects as $project) { ?>
                    { 'title' : '<?=$project->name?>', 'name' : 'work<?=$project->id?>', 'menu_button' : '0' },
                    <?php } ?>

                    { 'title' : 'About', 'name' : 'about', 'menu_button' : '1' },
                    { 'title' : 'Downloads', 'name' : 'downloads', 'menu_button' : '2' },
                    { 'title' : 'Contacts', 'name' : 'contacts', 'menu_button' : '3' }
                ];
            </script>

            <div id="wrap-index" class="b-page b-content_100" style="display: none;"></div>
            <div id="wrap-projects" class="b-page" style="display: none;"></div>

            <?php foreach ($projects as $project) { ?>
                <div id="wrap-work<?=$project->id?>" class="b-page" style="display: none;"></div>
            <?php } ?>

            <div id="wrap-about" class="b-page" style="display: none;"></div>
            <div id="wrap-downloads" class="b-page" style="display: none;"></div>
            <div id="wrap-contacts" class="b-page" style="display: none;"></div>
        </div>
        <!--------------------------------------------------------- #CONTENT ---------------------------------------------------------->

        <!---------------------------------------------------------- FOOTER ----------------------------------------------------------->
        <div class="b-footer" r-width="1400" style="display: none;">
            <div class="b-footer_in font-autosize" f-size="15">
                <div class="b-footer_in2">
                    <div class="b-block b-block_50">
                        <div class="b-footer_block_in">
                            <div class="b-footer_title b-footer_title_1">T. +7 (926) 450 4454</div>
                            <div class="b-footer_title b-footer_title_2">E. <a href="mailto:box@fieroanimals.com">box@fieroanimals.com</a></div>
                            <div class="b-footer_socials">
                                <a class="b-footer_social b-footer_social_tw" href="#"></a>
                                <a class="b-footer_social b-footer_social_fb" href="#"></a>
                                <a class="b-footer_social b-footer_social_be" href="#"></a>
                            </div>
                        </div>
                    </div>
                    <div class="b-block b-block_50">
                        <div class="b-footer_block_in">
                            <div class="b-footer_title b-footer_title_3">Send us a message</div>
                            <div class="b-footer_footnote"><span>*</span> All Fields Required</div>
                        </div>
                        <div class="b-footer_form">
                            <div class="b-footer_form_block">
                                <div class="b-footer_block_in2">
                                    <div class="b-footer_input_wrap"><input class="b-footer_input" type="text" placeholder="NAME" /></div>
                                    <div class="b-footer_input_wrap"><input class="b-footer_input" type="text" placeholder="COMPANY" /></div>
                                    <div class="b-footer_input_wrap"><input class="b-footer_input" type="text" placeholder="E-MAIL" /></div>
                                    <a class="b-footer_button_attach" href="#">Attach Brief</a>
                                </div>
                            </div>
                            <div class="b-footer_form_block">
                                <div class="b-footer_block_in2">
                                    <div class="b-footer_input_wrap"><textarea class="b-footer_input b-footer_textarea" placeholder="TEXT"></textarea></div>
                                    <div class="b-footer_input_wrap"><a class="b-footer_button_send" href="#">Send <span></span></a></div>
                                </div>
                            </div>
                            <div class="asd-clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!---------------------------------------------------------- #FOOTER ---------------------------------------------------------->

    </div>
</div>