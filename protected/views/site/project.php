<?php
/**
 * @var $project Projects
 */
?>
<div class="b-wrapper">

    <div class="b-header">
        <div class="b-wrap_in">
            <a href="#" class="b-header__img"><img src="<?=Yii::app()->getBaseUrl(true)?>/img/header-main.png" /></a>
            <div class="b-header__menu">
                <a href="<?=Yii::app()->createUrl('/')?>" class="b-header__menu_item">Home</a>
                <a href="<?=Yii::app()->createUrl('site/projects')?>" class="b-header__menu_item b-header__menu_item_active">Projects</a>
                <a href="<?=Yii::app()->createUrl('site/about')?>" class="b-header__menu_item">About</a>
                <a href="<?=Yii::app()->createUrl('site/contacts')?>" class="b-header__menu_item">Contacts</a>
            </div>
            <div class="asd-clear"></div>
        </div>
    </div>

    <div class="b-inner_content">
        <?php if (isset($project->images)) { foreach ($project->images as $project_img) { ?>
            <div class="b-inner_head" style="background-image: url('<?=Yii::app()->getBaseUrl(true).Projects::IMAGE_PATH.GHelper::dynamicPath($project_img->id)?>/<?=$project_img->id.'.'.$project_img->ext?>')"></div>
        <?php break; } } ?>

        <div class="b-inner_title"><?=$project->name?></div>
        <!--<div class="b-inner_title_sub">Dom.ru</div>-->

        <?php echo $project->text; ?>
    </div>

    <!--<div class="b-inner_content">
        <div class="b-inner_head"></div>

        <div class="b-inner_title">Rocket House</div>
        <div class="b-inner_title_sub">Dom.ru</div>

        <div class="b-wrap_in">
            <div class="b-inner_descr_block">
                <div class="b-inner_descr">
                    Fiero Animals is a creative retouching and CGi studio specializing in production of high quality commercial CG and photo
                    based imagery for the advertising and entertainment industries. We also provide a wide variety of services such as concept art,
                    creative direction, art direction, photo shooting and everything else you need in order to build a visual campaign from scratch
                    to a final visualization.<br/>
                    <br/>
                    For the past 5 years of experience we have built up an impressive client list that features many top advertising agencies and the
                    worlds' best known brands. We established a reputation of studio that delivers exceptional images on schedule and on budget.
                </div>
                <div class="b-inner_descr_items">
                    <div class="b-inner_descr_item"><b>Date:</b> 30th December 2012</div>
                    <div class="b-inner_descr_item"><b>Client:</b> Dom.ru</div>
                    <div class="b-inner_descr_item"><b>Art directed:</b> John Doe</div>
                    <div class="b-inner_descr_item"><b>Led and designed:</b> Peter Doe</div>
                    <div class="b-inner_descr_item"><b>3D-designed:</b> Steve Doe, Tom Doe, Peter Doe</div>
                </div>
                <div class="asd-clear"></div>
            </div>
        </div>

        <div class="b-inner_images_title">Making of Rocket House</div>
        <div class="b-inner_images_block">
            <div class="b-inner_images_item">
                <div class="b-inner_images_image"><img src="http://dvlp.cc/projects/fiero/img/design/02.png" /></div>
                <div class="b-inner_images_descr">
                    For the past 5 years of experience we have built up an impressive client
                    list that features many top advertising agencies and the worlds' best known brands.
                </div>
            </div>
            <div class="b-inner_images_item">
                <div class="b-inner_images_image"><img src="http://dvlp.cc/projects/fiero/img/design/03.png" /></div>
                <div class="b-inner_images_descr">
                    We established a reputation of studio that delivers exceptional images on schedule and on budget.
                </div>
            </div>
        </div>

        <div class="b-inner_text b-inner_center_text">
            Fiero Animals is a creative retouching and CGi studio specializing in production of high quality commercial CG
            and photo based imagery for the advertising and entertainment industries. We also provide a wide variety of services
            such as concept art, creative direction, art direction, photo shooting and everything else you need in order to build
            a visual campaign from scratch to a final visualization.
        </div>
        <div class="b-inner_text b-inner_center_text">
            For the past 5 years of experience we have built up an impressive client list that features many top advertising
            agencies and the worlds' best known brands. We established a reputation of studio that delivers exceptional images on schedule and on budget.
        </div>
        <div class="b-inner_text b-inner_center_text">
            Attention to detail and a creative approach to every project as long as a complex combination of high-end creative retouching and image
            manipulation with 3D digital imaging techniques are fundamentals of our workflow. For the past 5 years of experience we have built up an
            impressive client list that features many top advertising agencies and the worlds' best known brands. We established a reputation of studio
            that delivers exceptional images on schedule and on budget.
        </div>

        <div class="b-wrap_in2">
            <div class="b-inner_slider_block">
                <div class="b-inner_slider">
                    <div class="b-inner_slider_image asd-wrap"><img class="bg" src="http://dvlp.cc/projects/fiero/img/design/04.png" /></div>
                </div>
                <div class="b-inner_slider_text">
                    For the past 5 years of experience we have built up an impressive client list that features many top advertising agencies and the worlds'
                    best known brands. We established a reputation of studio that delivers exceptional images on schedule and on budget. Attention to detail
                    and a creative approach to every project as long as a complex combination of high-end creative retouching and image manipulation with 3D
                    digital imaging techniques are fundamentals of our workflow.<br/>
                    <br/>
                    We established a reputation of studio that delivers exceptional images on schedule and on budget. Attention to detail and a creative approach
                    to every project as long as a complex combination of high-end creative retouching and image manipulation with 3D digital imaging techniques
                    are fundamentals of our workflow.
                </div>
                <div class="asd-clear"></div>
            </div>
        </div>

        <div class="b-inner_text b-inner_center_text">
            Fiero Animals is a creative retouching and CGi studio specializing in production of high quality commercial CG and photo
            based imagery for the advertising and entertainment industries. We also provide a wide variety of services such as concept art,
            creative direction, art direction, photo shooting and everything else you need in order to build a visual campaign from scratch to a final visualization.
        </div>

        <div class="b-inner_footer_text b-inner_center_text">
            We established a reputation of studio that delivers exceptional images on schedule and on budget.
        </div>
    </div>-->

    <div class="b-footer b-footer_simple">
        <div class="b-wrap_in2">
            <?php
            $contacts = new Contacts();
            $attr = $contacts->findByPk(1);
            foreach($attr as $key=>$val) $contacts->$key = $val;
            ?>
            <a class="b-footer__email" href="mailto:<?=$contacts->email?>">box@fieroanimals.com</a>
            <div class="b-footer__tel"><?=$contacts->phone?></div>

            <div class="b-footer__socials">
                <?php
                $social = new ContactSocial();
                $allSocial = $social->findAll();
                foreach($allSocial as $newSocial){
                    ?>
                    <a class="" href="<?=$newSocial['link']?>"><img src="<?=Yii::app()->getBaseUrl(true)?><?=About::PATH_TO_IMAGE?>/<?=$newSocial['img']?>" width="20" height="20"></a>
                <?php
                }
                ?>
            </div>

            <div class="b-footer__download">
                    <span>
                        Downloads
                        <div class="b-download_panel_wrap" style="display: none;">
                            <em></em>
                            <div class="b-download_panel">
                                <div class="b-download_item"><a href="#">People Showcase</a>(17 Mb)</div>
                                <div class="b-download_item"><a href="#">Food & Drinks Showcase</a>(11 Mb)</div>
                                <div class="b-download_item"><a href="#">Transport Showcase</a>(20 Mb)</div>
                                <div class="b-download_item"><a href="#">Landscape Showcase</a>(24 Mb)</div>
                            </div>
                        </div>
                    </span>
            </div>

            <div class="asd-clear"></div>
        </div>
    </div>

</div>