<div class="b-index_content asd-wrap">
    <div class="b-index_slides asd-wrap">
        <?php
            foreach ($projects as $index => $project) {
                if ($index < 5) {
                    $proj_img = '';
//                    if (isset($project->images)) {
//                        foreach ($project->images as $project_img) {
//                            $proj_img = GHelper::getFullImagePath($project_img, 0);
//                            $proj_img = '<img class="bg" src="'.$proj_img.'" />';
//                            break;
//                        }
//                    }
                    $proj_img = GHelper::getFullImagePath($project, 0);
                    $proj_img = '<img class="bg" src="'.$proj_img.'" />';
        ?>
                    <div class="b-index_slide asd-wrap">
                        <?=$proj_img?>
                        <div class="b-index_slide_content">
                            <div class="b-index_slide_content_in">
                                <div class="b-index_slide_title">BBDO Moscow</div>
                                <div class="b-index_slide_text"><?=$project->short_name?></div>
                                <a class="b-index_slide_link b-slide_link" href="javascript:set_page('work<?=$project->id?>')">Take a look</a>
                            </div>
                        </div>
                    </div>
            <?php } ?>
        <?php } ?>
    </div>

    <div class="b-index_footer">
        <div class="b-index_footer_in">
            <div class="b-index_footer_links">
                <div class="b-index_footer_bg"></div>

                <?php foreach ($projects as $index => $project) { ?>
                    <?php if ($index < 5) { ?>
                        <div class="b-index_footer_link">
                            <div class="b-index_footer_link_text"><?=$project->name?><br/>BBDO Moscow</div>
                            <div class="b-index_footer_link_num">0<?=($index+1)?></div>
                        </div>
                    <?php } ?>
                <?php } ?>

                <div class="asd-clear"></div>
            </div>
        </div>
    </div>
</div>