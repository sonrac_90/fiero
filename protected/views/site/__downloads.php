<div class="b-main_bg b-red_bg">
    <div class="b-main_bg_title">Downloads</div>
    <div class="b-main_bg_line"></div>
    <div class="b-main_bg_text">Something you probably want to keep from us</div>
</div>

<div class="b-downloads_wrap">
    <div class="b-downloads_block b-aspect_ratio" data-aspect-ratio="26">
        <div class="b-block b-block_40 b-download_kit">
            <div class="b-download_kit_in">
                <div class="b-title b-download_kit_title">We carefully collected several sets of our work by common themes and a <span class="b-red_text">Press Kit</span></div>
                <div class="b-text b-download_kit_text">
                    <span class="b-line_in_text b-download_kit_line_in_text"></span>
                    You probably may be interested to download any specific part of our portfolio or the press kit for publication (please, let us know)
                </div>
            </div>
            <div class="b-download_kit_button_wrap"><a class="b-button b-download_kit_button" href="#">Press Kit 23 MB</a></div>
        </div>
        <div class="b-block b-block_20 b-download_pdf">
            <div class="b-download_pdf_in">
                <div class="b-download_pdf_img"><img class="asd-img-width" src="<?=Yii::app()->getBaseUrl(true)?>/img/downloads/01.png" /></div>
                <div class="b-download_pdf_title">Automotive</div>
                <div class="b-download_pdf_text">Cars, motorcycles, trains — mostly everythings on wheels</div>
            </div>
            <div class="b-download_pdf_button_wrap"><a class="b-button" href="#">Pdf 15 MB</a></div>
        </div>
        <div class="b-block b-block_20 b-download_pdf">
            <div class="b-download_pdf_in">
                <div class="b-download_pdf_img"><img class="asd-img-width" src="<?=Yii::app()->getBaseUrl(true)?>/img/downloads/02.png" /></div>
                <div class="b-download_pdf_title">Animals</div>
                <div class="b-download_pdf_text">Cars, motorcycles, trains — mostly everythings on wheels</div>
            </div>
            <div class="b-download_pdf_button_wrap"><a class="b-button" href="#">Pdf 15 MB</a></div>
        </div>
        <div class="b-block b-block_20 b-download_pdf">
            <div class="b-download_pdf_in">
                <div class="b-download_pdf_img"><img class="asd-img-width" src="<?=Yii::app()->getBaseUrl(true)?>/img/downloads/03.png" /></div>
                <div class="b-download_pdf_title">Food&Drink</div>
                <div class="b-download_pdf_text">Cars, motorcycles, trains — mostly everythings on wheels</div>
            </div>
            <div class="b-download_pdf_button_wrap"><a class="b-button" href="#">Pdf 15 MB</a></div>
        </div>
        <div class="asd-clear"></div>
    </div>
    <div class="b-downloads_block b-aspect_ratio" data-aspect-ratio="26">
        <div class="b-block b-block_20 b-download_pdf">
            <div class="b-download_pdf_in">
                <div class="b-download_pdf_img"><img class="asd-img-width" src="<?=Yii::app()->getBaseUrl(true)?>/img/downloads/04.png" /></div>
                <div class="b-download_pdf_title">Landscape</div>
                <div class="b-download_pdf_text">Cars, motorcycles, trains — mostly everythings on wheels</div>
            </div>
            <div class="b-download_pdf_button_wrap"><a class="b-button" href="#">Pdf 15 MB</a></div>
        </div>
        <div class="b-block b-block_20 b-download_pdf">
            <div class="b-download_pdf_in">
                <div class="b-download_pdf_img"><img class="asd-img-width" src="<?=Yii::app()->getBaseUrl(true)?>/img/downloads/05.png" /></div>
                <div class="b-download_pdf_title">People</div>
                <div class="b-download_pdf_text">Cars, motorcycles, trains — mostly everythings on wheels</div>
            </div>
            <div class="b-download_pdf_button_wrap"><a class="b-button" href="#">Pdf 15 MB</a></div>
        </div>
        <div class="b-block b-block_20 b-download_pdf">
            <div class="b-download_pdf_in">
                <div class="b-download_pdf_img"><img class="asd-img-width" src="<?=Yii::app()->getBaseUrl(true)?>/img/downloads/06.png" /></div>
                <div class="b-download_pdf_title">Misc</div>
                <div class="b-download_pdf_text">Cars, motorcycles, trains — mostly everythings on wheels</div>
            </div>
            <div class="b-download_pdf_button_wrap"><a class="b-button" href="#">Pdf 15 MB</a></div>
        </div>
        <div class="asd-clear"></div>
    </div>
</div>