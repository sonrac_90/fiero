<?php

/**
 * This is the model class for table "{{contacts_social}}".
 *
 * The followings are the available columns in table '{{contacts_social}}':
 * @property integer $id
 * @property string $name
 * @property string $img
 */
class ContactSocial extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{contacts_social}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, img, link, position', 'required'),
            array('position', 'numerical', 'integerOnly' => true),
            array('name, link', 'length', 'max'=>255),
            // The following rule is used by search().
            array('id, name, img, link, position', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    public function defaultScope(){
        return array(
            'order' => 'position ASC'
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'Номер',
            'name' => 'Социальная сеть',
            'img' => 'Иконка',
            'link' => 'Ссылка',
            'position' => 'Позиция'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('img',$this->img,true);
        $criteria->compare('link',$this->img,true);
        $criteria->compare('position',$this->img,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ContactSocial the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}