<?php

/**
 * This is the model class for table "project_images".
 *
 * The followings are the available columns in table 'project_images':
 * @property integer $id
 * @property integer $project_id
 * @property integer $position
 * @property string $name
 * @property string $date
 * @property string $ext
 */
class ProjectImages extends CActiveRecord
{
    public $images;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{project_images}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(

            //array('image', 'file', 'types' => 'jpg,jpeg,png,gif'),

			array('project_id, position', 'numerical', 'integerOnly' => true),
			array('name', 'length', 'max' => 255),
			array('ext', 'length', 'max' => 10),
			array('id, project_id, name, date, ext', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
            'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'         => 'ID',
			'project_id' => 'Проект',
			'name'       => 'Название',
			'date'       => 'Дата',
			'ext'        => 'Расширение',
			'position'   => 'Позиция',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('ext',$this->ext,true);
		$criteria->compare('position',$this->position,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProjectImages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
