<?php

    /**
     * This is the model class for table "{{description}}".
     *
     * The followings are the available columns in table '{{description}}':
     *
     * @property integer            $id
     * @property string             $title
     * @property string             $text
     * @property integer            $project_id
     * @property integer            $position
     * @property integer            $number
     * @property integer            $position_child
     *
     * The followings are the available model relations:
     * @property Projects           $project
     * @property DescriptionPhoto[] $descriptionPhotos
     */
    class Description extends CActiveRecord {
        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return '{{description}}';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array(
                    'project_id, position, position_child, number',
                    'numerical',
                    'integerOnly' => true
                ),
                array(
                    'title',
                    'length',
                    'max' => 255
                ),
                array(
                    'text',
                    'safe'
                ),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array(
                    'id, title, text, project_id',
                    'safe',
                    'on' => 'search'
                ),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'project'           => array(
                    self::BELONGS_TO,
                    'Projects',
                    'project_id'
                ),
                'descriptionPhotos' => array(
                    self::HAS_MANY,
                    'DescriptionPhoto',
                    'description_id',
                    'order' => 'descriptionPhotos.position ASC',
                ),
            );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'             => 'ID',
                'title'          => 'Заголовок',
                'text'           => 'Описание',
                'project_id'     => 'Проект',
                'position'       => 'Позиция блока',
                'number'         => 'Номер блока',
                'position_child' => 'Позиция в блоке',
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         *
         * Typical usecase:
         * - Initialize the model fields with values from filter form.
         * - Execute this method to get CActiveDataProvider instance which will filter
         * models according to data in model fields.
         * - Pass data provider to CGridView, CListView or any similar widget.
         *
         * @return CActiveDataProvider the data provider that can return the models
         * based on the search/filter conditions.
         */
        public function search() {
            // @todo Please modify the following code to remove attributes that should not be searched.

            $criteria = new CDbCriteria;

            $criteria->compare('id', $this->id);
            $criteria->compare('title', $this->title, true);
            $criteria->compare('text', $this->text, true);
            $criteria->compare('project_id', $this->project_id);
            $criteria->compare('position', $this->position);
            $criteria->compare('position_child', $this->position_child);
            $criteria->compare('number', $this->number);

            return new CActiveDataProvider(
                $this, array(
                    'criteria' => $criteria,
                )
            );
        }

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         *
         * @param string $className active record class name.
         *
         * @return Description the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }

        public static function getDescription($projectID) {
            $model = self::model();
            echo "<pre>";
            $data = $model->findAll('project_id = :projectID', array(':projectID' => $projectID));

            $returnData = array();

            $cnt = -1;
            $cntChild = 0;
            $previousPosition = 0;
            $retData = array();
            /** @var Description $_next */
            foreach ($data as $_next) {
                if ($previousPosition != $_next->position) {
                    $previousPosition = $_next->position;
                    $cnt++;
                    $cntChild = 0;
                }

                if (!isset($retData[$cnt])) {
                    $retData[ $cnt ] = array();
                }

                if (!isset($retData[$cnt][$_next->number]))
                    $retData[$cnt][$_next->number] = array();

                $retData[$cnt][$_next->number]['title'] = $_next->title;
                $retData[$cnt][$_next->number]['text'] = $_next->text;

                $retData[$cnt][$_next->number]['photo'] = array();

                if (!is_null($_next->descriptionPhotos))
                    foreach ($_next->descriptionPhotos as $photoD) {
                        $retData[$cnt][$_next->number]['photo'][] = array(
                            'id' => $photoD->id,
                            'ext' => $photoD->ext,
                        );
                    }

                $cntChild++;
            }

            return $retData;
        }
    }
