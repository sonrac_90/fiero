<?php

    /**
     * This is the model class for table "{{_projects}}".
     *
     * The followings are the available columns in table '{{_projects}}':
     *
     * @property integer          $id
     * @property string           $name
     * @property integer          $visible
     * @property string           $add_date
     * @property string           $update_date
     * @property integer          $preview_size
     * @property integer          $user_id
     * @property integer          $position
     * @property string           $image_ext
     * @property string           $short_name
     * @property string           $text
     * @property string           $credits
     * @property string           $agency
     * @property string           $client
     * @property string           $industry
     *
     * The followings are the available model relations:
     * @property ProjectFiles[]   $projectFiles
     * @property ProjectTags[]    $projectTags
     * @property ProjectImages[]  $images
     * @property ProjectCredits[] $projectCredits
     * @property Users            $user
     */
    class Projects extends CActiveRecord {

        const IMAGE_PATH              = '/uploads/images/original/';
        const IMAGE_PREVIEW_PATH      = '/uploads/images/preview/';
        const IMAGE_BIG_PATH          = '/uploads/images/big/';
        const IMAGE_SMALL_PATH        = '/uploads/images/small/';
        const IMAGE_PROJECT_PATH      = '/projects/';
        const IMAGE_SLIDER_PATH       = '/slider/'; /*****************/
        const IMAGE_TEMP_PATH         = '/uploads/images/temp/original/';
        const IMAGE_TEMP_PREVIEW_PATH = '/uploads/images/temp/preview/';
        const IMAGE_CONTENT_PATH      = '/uploads/content/';

        public $countPositions;

        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return '{{projects}}';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            return array(

                array(
                    'name, text',
                    'required'
                ),

                array(
                    'image_ext',
                    'required',
                    'message' => 'Необходимо загрузить фото'
                ),

                array(
                    'visible, preview_size, user_id, position',
                    'numerical',
                    'integerOnly' => true
                ),
                array(
                    'name',
                    'length',
                    'max' => 1024
                ),
                array(
                    'short_name, client, agency, industry',
                    'length',
                    'max' => 255
                ),
                array(
                    'image_ext',
                    'length',
                    'max' => 10
                ),
                array(
                    'id, name, visible, add_date, update_date, user_id, short_name, text, image_ext, agency, industry, client',
                    'safe',
                    'on' => 'search'
                ),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'descriptions'   => array(
                    self::HAS_MANY,
                    'Description',
                    'project_id',
                    'order' => 'descriptions.position ASC, position_child ASC'
                ),
                'projectFiles'   => array(
                    self::HAS_MANY,
                    'ProjectFiles',
                    'project_id'
                ),
                'projectTags'    => array(
                    self::HAS_MANY,
                    'ProjectTags',
                    'project_id'
                ),
                'projectCredits' => array(
                    self::HAS_MANY,
                    'ProjectCredits',
                    'project_id',
                    'order' => 'projectCredits.position ASC',
                ),
                'user'           => array(
                    self::BELONGS_TO,
                    'Users',
                    'user_id'
                ),
                'images'         => array(
                    self::HAS_MANY,
                    'ProjectImages',
                    'project_id',
                    'order' => 'images.position ASC',
                ),
            );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'           => 'ID',
                'name'         => 'Название',
                'visible'      => 'Видимость',
                'add_date'     => 'Дата добавления',
                'update_date'  => 'Дата обновления',
                'preview_size' => 'Размер превью',
                'user_id'      => 'Пользователь',
                'short_name'   => 'Короткое название',
                'text'         => 'Текст',
                'credits'      => 'Разработчики',
                'client'       => 'Клиент',
                'agency'       => 'Агенство',
                'industry'     => 'Отрасль',
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         *
         * Typical usecase:
         * - Initialize the model fields with values from filter form.
         * - Execute this method to get CActiveDataProvider instance which will filter
         * models according to data in model fields.
         * - Pass data provider to CGridView, CListView or any similar widget.
         *
         * @return CActiveDataProvider the data provider that can return the models
         * based on the search/filter conditions.
         */
        public function search() {
            $criteria = new CDbCriteria;

            $criteria->compare('id', $this->id);
            $criteria->compare('name', $this->name, true);
            //$criteria->compare('visible',$this->visible, true);
            $criteria->compare('add_date', $this->add_date, true);
            $criteria->compare('update_date', $this->update_date, true);
            //$criteria->compare('preview_size',$this->preview_size,true);
            $criteria->compare('user_id', $this->user_id);
            $criteria->compare('short_name', $this->short_name, true);
            $criteria->compare('text', $this->text, true);
            $criteria->compare('credits', $this->credits, true);
            $criteria->compare('industry', $this->industry, true);
            $criteria->compare('agency', $this->agency, true);
            $criteria->compare('client', $this->client, true);

            return new CActiveDataProvider(
                $this, array(
                    'criteria' => $criteria,
                )
            );
        }

        /**
         * @return bool
         */
        public function beforeSave() {
            if ($this->isNewRecord) {
                $criteria = new CDbCriteria();
                $criteria->select = 'COUNT(id) as countPositions';
                $count = self::find($criteria);
                $this->position = $count->countPositions + 1;
                $this->user_id = Yii::app()->user->id;
            }

            return parent::beforeSave();
        }

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         *
         * @param string $className active record class name.
         *
         * @return Projects the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }
    }
