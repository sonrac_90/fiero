
<?php

/**
 * This is the model class for table "{{sliders}}".
 *
 * The followings are the available columns in table '{{sliders}}':
 * @property integer $id
 * @property integer $project
 * @property integer $position
 *
 * The followings are the available model relations:
 * @property Projects $project0
 */
class Sliders extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{sliders}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('project, position', 'required'),
            array('project, position', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, project, position', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'project0' => array(self::BELONGS_TO, 'Projects', 'project'),
        );
    }


    public function defaultScope(){
        return array(
            'order' => 'position ASC'
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'project' => 'Project',
            'position' => 'Position',
        );
    }

    public function createList(){
        $result = array();
        $allSliders = $this->findAll();
        foreach($allSliders as $value){
            $nextVal = array();
            $nextVal['attr'] = $value['attributes'];
            $this->attributes = $value['attributes'];

            $this->id = $value['attributes']['id'];
            $project = Projects::model()->findByPk($value['attributes']['project']);

            $fileName = $project->id . '.' . $project->image_ext;
            $dp = GHelper::dynamicPath($project->id);
            $prevUrl = Yii::app()->baseUrl . Projects::IMAGE_PREVIEW_PATH . Projects::IMAGE_PROJECT_PATH . $dp . '/' . $fileName;
            $nextVal['imageURL'] = $prevUrl;
            $nextVal['linkProject'] = Yii::app()->createAbsoluteUrl("/site/projects/id" . $project->id);
            $nextVal['nameProject'] = $project->name;
            $nextVal['linkProject'] = Yii::app()->createAbsoluteURL("adminx24/projects") . "/update/id/" . $project->id;
            $result[] = $nextVal;
            $this->unsetAttributes();
            unset($this->attributes);
        }

        return $result;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('project',$this->project);
        $criteria->compare('position',$this->position);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Sliders the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}