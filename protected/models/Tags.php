<?php

/**
 * This is the model class for table "{{_tags}}".
 *
 * The followings are the available columns in table '{{_tags}}':
 * @property integer $id
 * @property integer $position
 * @property string $name
 * @property string $tag_id
 * @property integer $visible
 * @property string $date
 *
 * The followings are the available model relations:
 * @property ProjectTags[] $projectTags
 */
class Tags extends CActiveRecord
{
    public $countPositions;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tags}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('visible, position', 'numerical', 'integerOnly' => true),
			array('name, tag_id', 'length', 'max' => 255),
			array('id, name, tag_id, visible, date', 'safe', 'on' => 'search'),
		);
	}

    public static function tagList()
    {
        $array = array();
        $criteria = new CDbCriteria();
        $criteria->addCondition('visible = 1');
        $criteria->order = 'position DESC';
        $tags = self::model()->findAll();
        if($tags)
        {
            foreach($tags as $tag)
            {
                $array[$tag->id] = $tag->name;
            }
        }
        return $array;
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'projectTags' => array(self::HAS_MANY, 'ProjectTags', 'tag_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Имя',
			'tag_id' => 'Tag ID',
			'visible' => 'Видимость',
			'date' => 'Дата добавления',
		);
	}

    public function beforeSave()
    {
        if($this->isNewRecord)
        {
            $criteria = new CDbCriteria();
            $criteria->select = 'COUNT(id) as countPositions';
            $count = self::find($criteria);
            $this->position = $count->countPositions + 1;
        }
        return parent::beforeSave();
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('tag_id',$this->tag_id,true);
		$criteria->compare('visible',$this->visible);
		$criteria->compare('date',$this->date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tags the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
