$(document).ready(function(){
    $('.sliders').sortable({});
    $('.sliders').on("sortstop", function( event, ui ) {
        var a = $('.sliders').sortable('toArray');
        var id = {idReal: {}};
        for (var i = 0; i < a.length - 1; i++){
            var num = a[i].replace('slide', '').split('_');
            id.idReal[i] = num[0];
        }
        $.ajax({
            method: 'POST',
            url: admin_url + '/sliders/changeindex',
            data: id,
            success: function (data){
            },
            error: function (xhr, err){

            }
        })
    } );

    $('.sliders .delete').each(function (){
        $(this).on('click', function (){
            var number = $(this).parent().attr('id').replace('slide', '').split('_');
            $(this).attr('id', 'removeFromPage');
            $.ajax ({
                url: admin_url + '/sliders/delete/id/' + number[0],
                success: function (data){
                    $('#removeFromPage').parent().remove();
                },
                error : function (xhr, err){
                    $('#removeFromPage').removeAttr('id');
                }
            });
            return false;
        });
    });

})