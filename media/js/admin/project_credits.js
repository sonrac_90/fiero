$(document).ready(function(){
    var creditsCount = 0;
    var addCredit = $('#addCredit');

    addCredit.click(function(){
        creditsCount++;
        var creditBlock = '<div class="credits-block">' +
            '<i class="glyphicon glyphicon-remove"></i>' +
            '<div class="inputs">' +
                '<div>' +
                    '<span>Должность</span>'+
                    '<input type="text" name="creditsName[]" class="form-control"/>'+
                '</div>'+
                '<div>'+
                    '<span>Фамилия, имя</span>'+
                    '<input type="text" name="creditsValue[]" class="form-control"/>'+
                '</div>'+
//                '<div>'+
//                    '<span>ссылка</span>'+
//                    '<input type="text" name="creditsUrl[]" class="form-control"/>'+
//                '</div>'+
            '</div>' +
            '</div>';
        $('.credits-blocks').append(creditBlock);
        deleteCredits();
    });
    deleteCredits();

    $('.credits-blocks').sortable();

});