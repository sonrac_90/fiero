/**
 * Created by sonrac on 31.01.14.
 */
$(document).ready(function (){
    $('#fileAbout').on('change', function (){
        var form = document.querySelector('form[id^="socialForm"]');
        var formData = new FormData(form);

        $.ajax({
            type: 'POST',
            url: admin_url + "/default/loadimage",
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(data) {
                $(".image img").attr('src', data.image_url);
                $('form #socialImageLink').val(data.imageName);
            },
            error: function(data) {
                console.log('error', data);
            }
        });

    });
});