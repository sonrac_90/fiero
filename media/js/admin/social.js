/**
 * Created by sonrac on 30.01.14.
 */
$(document).ready(function(){
    $('.social').sortable({});
    $('.social').on("sortstop", function( event, ui ) {
        var a = $('.social').sortable('toArray');
        var id = {idReal: {}};
        for (var i = 0; i < a.length - 1; i++){
            var num = a[i].replace('num', '').split('_');
            id.idReal[i] = num[0];
        }
        $.ajax({
            method: 'POST',
            url: admin_url + '/default/changeindex',
            data: id,
            success: function (data){
            },
            error: function (xhr, err){

            }
        })
    } );

    var successIMgLoad = function (data){
        $(".image img").attr('src', data.image_url);
        $('form #socialImageLink').val(data.imageName);
    }

    var successAdd = function (data){
        $('#addNew #error').remove();
        if (typeof data.success !== 'undefined'){
            var div = $('<div></div>', {'id' : 'error'}).html('Заполните все поля!!!');
            $('#addNew').prepend(div);
            return false;
        }

        var divNewElem = $('<div/>', {className: "", id: 'num' + data.id + '_' + data.position}).addClass('rowSocial')
            .append($('<span/>').html(data.name))
            .append($('<span/>').append($('<img/>', {src: admin_url + '/../uploads/images/content/' + data.img, width: 20, height: 20})))
            .append($('<span/>').append($('<a/>', {href: data.link}).html(data.link)))
            .append($('<span/>').addClass('button-column')
                .append($('<a/>', {href: admin_url + '/default/update/id/' + data.id}).html('Изменить').addClass('glyphicon glyphicon-pencil'))
                .append($('<a/>', {href: admin_url + '/default/delete/id/' + data.id}).html('Удалить').addClass('glyphicon glyphicon-pencil'))
            )

        $(".social").append(divNewElem);
        $('#addNew').html('');
        return false;
    }

    var sendData = function (event){
        var form = document.querySelector('form[id^="socialForm"]');
        var formData = new FormData(form);

        var url = admin_url + '/default/add';
        var callback = successAdd;
        if (this.tagName.toLowerCase() == 'input'){
            url = admin_url + "/default/loadimage";
            callback = successIMgLoad;
        }
        $.ajax({
            type: 'POST',
            url: url,
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: callback,
            error: function(data) {
                console.log('error', data);
            }
        });
        return false;
    };

    var send = function (){
        $('#addNew').html('');
    }

    $('#addSocial').on('click', function (){
//        document.location.href = admin_url + "/default/add";
        var html = '<div class="content"><form id="socialForm" action="/adminx24/default/add" method="post"><div class="social"><div class="social"><div class="name"><div><label for="ContactSocial_name">Социальная сеть</label></div><div><input class="form-control" name="ContactSocial[name]" id="ContactSocial_name" type="text" maxlength="255"></div><div></div>';
        html+='</div><div class="image"><div><label for="ContactSocial_img">Иконка</label></div><div><input class="form-control" id="socialImageLink" name="ContactSocial[img]" type="hidden"></div><div><img width="50" height="50" src="/uploads/images/content/" alt="Изображение"></div><div id="" style="text-align: center; overflow: hidden; width: 150px; height: 50px; border: 1px solid #038f1a; background-color: #038f1a;"><div style="color: #fff; font-size: 20px; font-weight: bold;">Выбрать файл</div><input id="fileAbout" name="file" style="margin-top: -50px; margin-left:-410px;-moz-opacity: 0; filter: alpha(opacity=0); opacity: 0; font-size: 150px; height: 100px;" type="file" value=""></div>';
        html +='</div><div class="link"><div><label for="ContactSocial_link">Ссылка</label></div><div><input class="form-control" name="ContactSocial[link]" id="ContactSocial_link" type="text" maxlength="255"></div><div></div>';
        html += '<div><input class="form-control" name="ContactSocial[position]" id="ContactSocial_position" type="hidden" value="4"></div><div></div></div><input class="btn btn-success" type="submit" name="yt0" value="Сохранить"></div>            </div></form></div>';
        $('#addNew').html(html);

        $('#fileAbout').on('change', sendData);
        $('#addNew form').on('submit', sendData);

    });
})