$(document).ready(function () {
    Init();
});

function Init() {
    initTinyMCE();
    removeDescription();
    addDescription();
    addDescriptionChild();
}

function addDescription () {
    $('#addDescription').off('click').on('click', function (event) {
        event.preventDefault();
        var count = $('body').find('.descriptionBlock').size() + 1;
        var countDescription = $('fieldset legend').size() + 1;
        var fieldSet = $('fieldset').has(this);
        var collectionInput = fieldSet.find('.text').eq(-1).find('input[type^=hidden]');
        var position = collectionInput.eq(-1).val();

        if (typeof position === "undefined")
            position = 0;

        position++;

        var html = $('<fieldset/>')
                .append($('<legend/>').html('Описание ' + countDescription)
                           .append($('<i/>').addClass('glyphicon glyphicon-remove deleteDescription')))
                .append($('<div/>').addClass('descriptionBlock').text('Блок 1')
                   .append($('<i/>').addClass('glyphicon glyphicon-remove deleteDescription'))

                   .append($('<div/>')
                            .append($('<input/>', {name : 'Description[' + count + '][title]', id : 'Description_title'}).addClass('form-control'))
                   )
                   .append($('<div/>')
                            .append($('<div/>', {id : 'Description_title_em_', style : 'display: none'}).addClass('errorMessage'))
                   )
                   .append($('<div/>')
                            .append($('<label/>', {for : 'Projects_text'}).text('Текст'))
                   )
                   .append($('<div/>').addClass('text')
                            .append($('<textarea/>', {id : 'selector_' + count, name : 'Description[' + count + '][text]'}).addClass('input_tinymce form-control'))
                            .append($('<input/>', {name : 'Description[' + count + '][number]', type: 'hidden'}).val(1))
                            .append($('<input/>', {name : 'Description[' + count + '][position]', type: 'hidden'}).val(count))
                            .append($('<input/>', {name : 'Description[' + count + '][position_child]', type: 'hidden'}).val(position))
                            .append($('<div/>').append($('<div/>', {id : 'Description_title_em_', style : 'display : none;'})
                            )))
                )
                .append($('<div/>').append($('<br/>')).append($('<div/>').attr({'data-key' : count}).addClass('images')))
                .append($('<div/>').addClass('clearFix'))
                .append($('<input/>', {type: 'file'}).addClass('fileUpload'))
                .append($('<hr/>'))
                .append($('<fieldset/>').append($('<button/>').addClass('btn btn-success addText').html('Добавить текстовое поле')))
                .append($('<hr/>'));


        $(this).before(html);
        addDescriptionChild();
        Init();
        initSortable();
        var selector = $(this).parent().find('.fileUpload').eq(-1);
        pekeUploadEvent(actionUploadDescription, selector);
        return false;
    });
}

function addDescriptionChild (selector) {
    if (typeof selector === 'undefined') {
        selector = '.addText';
    }

    $(selector).off('click').on('click', function (event) {
        event.preventDefault();
        var fieldSet = $('fieldset').has(this);
        var count = $('body').find('.descriptionBlock').size() + 1;
        var collectionInput = fieldSet.find('.text').eq(-1).find('input[type^=hidden]');
        var position = collectionInput.eq(-1).val();
        var positionChild = collectionInput.eq(collectionInput.length - 2).val();
        var countBlock = fieldSet.find('.descriptionBlock').size() + 1;
        var html = $('<div/>').addClass('descriptionBlock').html('<hr/>Блок ' + countBlock)
                        .append($('<i/>').addClass('glyphicon glyphicon-remove deleteDescription'))
                        .append($('<div/>')
                                    .append($('<input/>', {name : 'Description[' + count + '][title]', id : 'Description_title'}).addClass('form-control'))
                        )
                        .append($('<div/>')
                                    .append($('<div/>', {id : 'Description_title_em_', style : 'display: none'}).addClass('errorMessage'))
                        )
                        .append($('<div/>')
                                    .append($('<label/>', {for : 'Projects_text'}).text('Текст'))
                        )
                        .append($('<div/>').addClass('text')
                                    .append($('<textarea/>', {id : 'selector_' + count, name : 'Description[' + count + '][text]'}).addClass('input_tinymce form-control'))
                                    .append($('<input/>', {name : 'Description[' + count + '][number]', type: 'hidden'}).val(countBlock))
                                    .append($('<input/>', {name : 'Description[' + count + '][position]', type: 'hidden'}).val(position))
                                    .append($('<input/>', {name : 'Description[' + count + '][position_child]', type: 'hidden'}).val(positionChild))
                                    .append($('<div/>')
                                              .append($('<div/>', {id : 'Description_title_em_', style : 'display : none;'}))
                                           )
                                   .append($('<div/>').append($('<br/>')).append($('<div/>').attr({'data-key' : count}).addClass('images'))                                   .append($('<input/>', {type: 'file'}).addClass('fileUpload'))
                                                   )

                                                       .append($('<div/>').addClass('clearFix'))
                        );

        var selector = $(fieldSet).has(this).eq(0).find('.descriptionBlock').eq(-1);

        selector = $('fieldSet').has(this).eq(0).find('hr').eq(-2);

        console.log(selector);
        $(selector).before(html);

        Init();
        initSortable();
        var selector = $(html).find('.fileUpload');
        pekeUploadEvent(actionUploadDescription, selector);
        return false;
    });
}

function removeDescription(selector) {
    if (typeof selector === "undefined") {
        selector = '.deleteDescription';
    }
    $(selector).each(function () {
        $(this).off('click').on('click', function (event) {
            event.preventDefault();
            var self = this;
            var confirmDelete = confirm("Удалить элемент?");
            if (confirmDelete) {
                var id = $(self).attr('data');
                if (id) {
                    var parent = $(this).attr('data-parent');
                    if (parent)
                        parent = "/parent/" + parent;
                    else
                        parent = '';
                    $.ajax({
                               url     : admin_url + '/projects/deleteDescription/id/' + id + parent,
                               type    : 'POST',
                               success : function (data) {
                                   if (data) {
                                       alert('Ошибка : ' + data);
                                       return ;
                                   }
                                   deleteDescription(self);
                               },
                               error   : function (xhr, error) {
                                   console.log(xhr, error);
                               }
                    });
                } else {
                    deleteDescription(self);
                }
            }
            removeDescription();
            return false;
        });
    });
}

function deleteDescription (elem) {
    var parent = $('legend').has(elem);
    if (!parent.size())
        parent = $('div.descriptionBlock').has(elem);
    else
        parent = parent.parent();

    if (parent) {
        $(parent).remove();
    }
    updateNumber();
}

function updateNumber() {
    var cnt = 0;
    $('fieldset legend').each(function () {
        cnt++;

        $(this).html('Описание ' + cnt + '<i class="glyphicon glyphicon-remove"></i>');

        $('[name^=Description]').each(function () {
            var link = $(this).attr('name');
            $(this).attr('name', link.replace(/[\d]+/gmi, cnt))
        });

    });
}

function initTinyMCE () {

//    tinymce.init({
//                     selector           : "textarea",
//                     viewUrl            : admin_url + '/projects/uploadImage',
//                     plugins            : [
//                         "advlist autolink lists link image charmap print preview anchor",
//                         "searchreplace visualblocks code fullscreen",
//                         //"insertdatetime media table contextmenu paste moxiemanager"
//                         "insertdatetime media table contextmenu paste",
//                         "dvlpUploader",
//                         "youtube"
//                     ],
//                     language           : 'ru',
//                     relative_urls      : false,
//                     convert_urls       : false,
//                     remove_script_host : false,
//                     toolbar            : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | dvlpUploader | youtube"
//    });
}

function deleteCredits() {
    $('div.credits-block i').each(function () {
        $(this).off('click').on('click', function (event) {
            var promt = confirm("Удалить сотрудника?");

            if (promt) {
                var self = this;
                var id = $(this).attr('data');

                if (id) {
                    $.ajax({
                               url     : admin_url + '/projects/deleteCredits/id/' + id,
                               type    : 'POST',
                               success : function (data) {
                                   if (data) {
                                       alert('Ошибка: ' + data);
                                       return;
                                   }
                                   deleteElemCredits(self);
                               },
                               error   : function (xhr, error) {

                               }
                           });
                } else {
                    deleteElemCredits(self);
                }
            }
        });
    });
}

function deleteElemCredits(self) {
    var parent = $(self).parent();

    if (parent)
        parent.remove();
}