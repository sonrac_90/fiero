$(document).ready(function(){

    var $imagesCount = 0;
    var Gallery = new plupload.Uploader({
        runtimes : 'html5,flash,silverlight,html4',
        browse_button : 'imagesBrowseFile',
        container: 'images_container',
        url : admin_url + '/projects/uploadGallery',

        // Flash settings
        flash_swf_url : 'http://rawgithub.com/moxiecode/moxie/master/bin/flash/Moxie.cdn.swf',
        // Silverlight settings
        silverlight_xap_url : 'http://rawgithub.com/moxiecode/moxie/master/bin/silverlight/Moxie.cdn.xap',

        file_data_name: 'ImageGallery',

        multi_selection: true,

        filters : {
            max_file_size : '10mb',
            mime_types: [
                {
                    title : "Файл с изображением",
                    extensions : "jpg,gif,png,jpeg"
                }
            ]
        },

        init: {
            PostInit: function() {

            },

            FilesAdded: function(up, files) {
                Gallery.start();
            },

            FileUploaded: function(up, file, info) {

                var $json = JSON.parse(info.response);
                var $imageDiv = $('<div>');
                var $image = $('<img>');
                var $input = $('<input>');
                if($json.status == 'success')
                {
                    $imagesCount++
                    $imageDiv.append($image).append($input);
                    $imageDiv.addClass('project_image');
                    $image.attr('src', $json.image_url);
                    $image.attr('width', '109px');
                    $image.attr('height', '77px');
                    $input.attr('type','hidden');
                    $input.attr('name','project_images['+$imagesCount+']');
                    $input.attr('value',$json.image_id);

                    $('.project_images .images').append($imageDiv);
                }
            },

            UploadProgress:function(up, file)
            {
                $('.fonTypeFile').text('Загрузка ' + file.percent + '%');
            },

            Error: function(up, err) {
                $('#errorList').html("\nError #" + err.code + ": " + err.message);
            }
        }
    });

    var ProjectImage = new plupload.Uploader({
        runtimes : 'html5,flash,silverlight,html4',
        browse_button : 'projectBrowseFile',
        container: 'project_image_container',
        url : admin_url + '/projects/uploadPreview',

        // Flash settings
        flash_swf_url : 'http://rawgithub.com/moxiecode/moxie/master/bin/flash/Moxie.cdn.swf',
        // Silverlight settings
        silverlight_xap_url : 'http://rawgithub.com/moxiecode/moxie/master/bin/silverlight/Moxie.cdn.xap',

        file_data_name: 'imagePreview',

        multi_selection: false,

        filters : {
            max_file_size : '10mb',
            mime_types: [
                {
                    title : "Файл с изображением",
                    extensions : "jpg,gif,png,jpeg"
                }
            ]
        },

        init: {
            PostInit: function() {

            },

            FilesAdded: function(up, files) {
                ProjectImage.start();
            },

            FileUploaded: function(up, file, info) {

                var $json = JSON.parse(info.response);
                var $imageDiv = $('<div>');
                var $image = $('<img>');
                var $input = $('<input>');
                if($json.status == 'success')
                {
                    $imageDiv.append($image).append($input);
                    $imageDiv.addClass('prev_image');
                    $image.attr('src', $json.image_url);
                    $image.attr('width', '109px');
                    $image.attr('height', '77px');
                    $input.attr('type','hidden');
                    $input.attr('name','project_prev_image');
                    $input.attr('value',$json.original);

                    $('.project_image .image_preview').html($imageDiv);
                }
            },

            UploadProgress:function(up, file)
            {
                $('.fonTypeFile').text('Загрузка ' + file.percent + '%');
            },

            Error: function(up, err) {
                $('#errorList').html("\nError #" + err.code + ": " + err.message);
            }
        }
    });

    Gallery.init();
    ProjectImage.init();

    $('.project_images .images').sortable();

});

function uploadImageProjectDescription () {
    $.ajax({
        url : admin_url + '/projects/UploadGalleryDescription',
        type: 'POST'
           });
}