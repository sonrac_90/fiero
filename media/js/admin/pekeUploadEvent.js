var actionUploadDescription = admin_url + '/projects/uploadGalleryDescription';
$(document).ready(function () {
    var selector = $('.fileUpload');
    pekeUploadEvent(actionUploadDescription, selector);
    initDelete();
    initSortable();
});

function initSortable () {
    $('.images').sortable({
                              update               : function () {
                                  var cnt = 0;

                                  $(this).find('.sortDiv').each(function () {
                                      $(this).find('input[type^=hidden]').each(function () {
                                          var name = $(this).attr('name');
                                          $(this).attr('name', name.replace(/\[DescriptionPhotos\]\[[\d]\]+/gmi, '[DescriptionPhotos][' + cnt + ']'));
                                      });
                                      cnt++;
                                  });
                              }
                          }).disableSelection();
}

function initDelete() {
    $('.deletePhoto').off('click').on('click', function (event) {
        event.preventDefault();
        var self = this;
        var promt = confirm("Удалить изображение?");

        if (promt) {
            $.ajax ({
                        url : admin_url + '/projects/deletePhotoDescription/id/' + $(self).attr('data'),
                        success : function (data) {
                            if (!data) {
                                $(self).parent().remove();
                            }
                        }
                    });
        }
        return false;
    });
}

function pekeUploadEvent(action, elem, callbackSuccess, text, callbackError) {

    $(elem).each(function () {
        self = this;

        if (typeof (callbackError) === 'undefined') {
            callbackError = function (file, err) {
                console.log(file, err);
            }
        }

        if (typeof callbackSuccess === 'undefined')
            callbackSuccess = function (file, data) {
                var blockImage = $(file.contestElem).parent().find('.images').eq(0);
                var numDescription = $(blockImage).attr('data-key');
                var numElem = $(blockImage).find('img').size();
                var input = $('<input/>', {type: 'hidden'  , name: 'Description[' + numDescription + '][DescriptionPhotos][' + numElem + '][ext]' }).val(data.image_url);
                var inputID = $('<input/>', {type: 'hidden', name: 'Description[' + numDescription + '][DescriptionPhotos][' + numElem + '][id]' }).val(data.image_id);
                var imgTags = $('<img/>', {src: data.image_url});
                var div = $('<div/>').addClass('sortDiv')
                    .append(input)
                    .append(inputID)
                    .append(imgTags)
                    .append($('<br/>'))
                    .append($('<div/>')
                                  .attr('data', data.image_id)
                                  .addClass('btn btn-danger deletePhoto').html('Удалить')
                    );
                $(blockImage)
                    .append(div);
                initDelete();
            };

        if (typeof(text) === 'undefined') {
            text = "Добавить изображение";
        }

        $(this).pekeUpload2({
                                btnText                    : text,
                                theme                      : 'bootstrap',
                                showPercent                : true,
                                showErrorAlerts            : true,
                                allowedExtensions          : 'jpg|jpeg|png',
                                invalidExtError            : 'Неверный формат файла',
                                maxSize                    : 10,
                                field                      : 'ImageGallery',
                                sizeError                  : "Размер файла превышает установленный " +
                                                             "лимит в 10 Мб",
                                url                        : action,
                                dataType                   : 'JSON',
                                multiFileUpload            : false,
                                mimeType                   : 'image/*',
                                additionCheckCorrectAnswer : function (data) {
                                    console.log(data);
                                    if (data.status == 'success') {
                                        return true;
                                    }

                                    if (data.textError !== 'undefined')
                                        return data.textError;

                                    return false;
                                },
                                onFileSuccess              : function (file, data) {
                                    $(elem).html('Изменить');
                                    callbackSuccess(file, data);
                                },
                                onFileError                : function (file, error) {
                                    if (typeof callbackSuccess !== 'undefined')
                                        callbackError(file, error);
                                }
                            });
    });

}