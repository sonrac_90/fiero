$(document).ready(function (){
    $('#fileAbout').on('change', function (){
        var form = document.querySelector('form[id^="formAbout"]');
        var formData = new FormData(form);

        $.ajax({
            type: 'POST',
            url: admin_url + "/about/loadimage",
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(data) {
                $(".about_image img").attr('src', data.image_url);
                $('form #About_imgLink').val(data.imageName);
            },
            error: function(data) {
                console.log('error', data);
            }
        });

    });
})