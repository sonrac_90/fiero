$(document).ready(function(){
    /*$('#tag_inputs').tokenfield({

    });*/

    $('#tags').change(function(){
        var id = $('#tags option:selected').val();
        var name = $('#tags option:selected').text();

        var tagLabel = $('<label>');
        var input = $('<input>');
        var tagI = $('<i>');

        tagLabel.addClass('label label-primary');
        tagLabel.text(name);
        tagLabel.append(input);
        tagLabel.append(tagI);

        tagI.addClass('glyphicon glyphicon-remove');

        input.attr('type','hidden');
        input.val(id);
        input.attr('name','projectTags[]');

        $('.tags .tagList .items').append(tagLabel);
    });

    $('body').find('.tagList label.label i.glyphicon-remove').click(function(){
        $(this).parent().remove();
    });

});