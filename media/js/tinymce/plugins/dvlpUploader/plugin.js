/**
 * Created by Eugene on 20.01.14.
 */


tinymce.PluginManager.add('dvlpUploader', function(editor, url){

    function FileDialog()
    {
        var viewUrl = tinyMCE.activeEditor.getParam("viewUrl");
        editor.windowManager.open({
            'title' : 'Загрузка фото',
            viewUrl : viewUrl,
            width: 350,
            height: 150,
            buttons: [
                {
                    text: 'Загрузить',
                    classes: 'widget btn primary first abs-layout-item',
                    disabled: true,
                    onclick: 'close'
                },
                {
                    text: 'Закрыть',
                    onclick:'close'
                }
            ]
        });
    }

    editor.addButton('dvlpUploader', {
        tooltip: 'Загрузить изображение',
        icon: 'image',
        text: 'Загрузить',
        onclick: FileDialog
    });

    editor.addMenuItem('dvlpUploader', {
        text: 'Загрузить изображение',
        icon: 'image',
        context: 'insert',
        onclick: FileDialog
    });

});